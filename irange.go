// irange defines an object representing a union of intervals of int64s.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package irange

import (
	"bitbucket.org/pcas/irange/errors"
	"bitbucket.org/pcastools/bytesbuffer"
	"bitbucket.org/pcastools/stringsbuilder"
	"math/rand"
	"strconv"
	"strings"
)

// Range defines a union of intervals \bigcup_i [a_i, b_i] \cap \ZZ of integers.
type Range interface {
	// IsEmpty returns true if and only if the range is empty.
	IsEmpty() bool
	// Intervals returns a slice of intervals representing the range. Any
	// empty intervals will be removed, and any intervals that can be
	// combined into a single interval will be combined.
	Intervals() []Interval
}

// irange represents a disjoint union of intervals \bigcup_i [a_i, b_i] \cap \ZZ of integers.
type irange []Interval

/////////////////////////////////////////////////////////////////////////
// irange functions
/////////////////////////////////////////////////////////////////////////

// IsEmpty returns true if and only if this range is empty.
func (r irange) IsEmpty() bool {
	return len(r) == 0
}

// Intervals returns a slice of intervals representing this range. Any empty intervals will be removed, and any intervals that can be combined into a single interval will be combined.
func (r irange) Intervals() []Interval {
	S := make([]Interval, len(r))
	copy(S, r)
	return S
}

// String returns a string representation of the range.
func (r irange) String() string {
	b := stringsbuilder.New()
	defer stringsbuilder.Reuse(b)
	b.WriteByte('[')
	for i, I := range r {
		if i != 0 {
			b.WriteByte(',')
		}
		min, max := I.Min(), I.Max()
		b.WriteString(strconv.FormatInt(min, 10))
		if min != max {
			b.WriteByte('.')
			b.WriteByte('.')
			b.WriteString(strconv.FormatInt(max, 10))
		}
	}
	b.WriteByte(']')
	return b.String()
}

// MarshalJSON returns a JSON description of the range.
func (r irange) MarshalJSON() ([]byte, error) {
	// Fetch a bytes builder
	b := bytesbuffer.New()
	defer bytesbuffer.Reuse(b)
	// Write out the opening bracket
	b.WriteByte('[')
	// Write out the values
	for i, I := range r {
		// Write out a leading ',' if necessary
		if i != 0 {
			b.WriteByte(',')
		}
		// Write out the interval
		min, max := I.Min(), I.Max()
		if min == max {
			b.WriteString(strconv.FormatInt(min, 10))
		} else {
			b.WriteByte('[')
			b.WriteString(strconv.FormatInt(min, 10))
			b.WriteByte(',')
			b.WriteString(strconv.FormatInt(max, 10))
			b.WriteByte(']')
		}
	}
	// Write out the closing bracket
	b.WriteByte(']')
	// Return a copy of the underlying bytes
	raw := b.Bytes()
	cpy := make([]byte, len(raw))
	copy(cpy, raw)
	return cpy, nil
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// Len returns the number of integers contained in the range.
func Len(R Range) int {
	if R.IsEmpty() {
		return 0
	} else if I, ok := R.(Interval); ok {
		return int(I.Max() - I.Min() + 1)
	}
	n := 0
	for _, I := range R.Intervals() {
		n += int(I.Max() - I.Min() + 1)
	}
	return n
}

// Contains returns true if and only if the range contains n.
func Contains(R Range, n int64) bool {
	if R.IsEmpty() {
		return false
	} else if I, ok := R.(Interval); ok {
		return I.Min() <= n && n <= I.Max()
	}
	ok, _ := findInterval(R.Intervals(), n)
	return ok
}

// Span returns the smallest interval containing the range.
func Span(R Range) Interval {
	if R.IsEmpty() {
		return Empty
	} else if I, ok := R.(Interval); ok {
		return I
	}
	Is := R.Intervals()
	return ToInterval(Is[0].Min(), Is[len(Is)-1].Max())
}

// Contents returns the contents of the range as a slice.
func Contents(R Range) []int64 {
	if R.IsEmpty() {
		return nil
	} else if I, ok := R.(Interval); ok {
		a, b := I.Min(), I.Max()
		S := make([]int64, 0, int(b-a+1))
		for i := a; i <= b; i++ {
			S = append(S, i)
		}
		return S
	}
	S := make([]int64, 0)
	for _, I := range R.Intervals() {
		a, b := I.Min(), I.Max()
		for i := a; i <= b; i++ {
			S = append(S, i)
		}
	}
	return S
}

// Random returns a random element from the range, with a uniform distribution. If the range is empty this always returns 0.
func Random(R Range) int64 {
	if R.IsEmpty() {
		return 0
	} else if I, ok := R.(Interval); ok {
		a, b := I.Min(), I.Max()
		if a == b {
			return a
		}
		return a + rand.Int63n(b-a+1)
	}
	// Fetch the total number of elements in the range
	Is := R.Intervals()
	var size int64
	for _, I := range Is {
		size += I.Max() - I.Min() + 1
	}
	// Pick a value uniformly at random in this total
	j := rand.Int63n(size)
	// Locate the corresponding element of the range
	for _, I := range Is {
		k := I.Max() - I.Min() + 1
		if j < k {
			return I.Min() + j
		}
		j -= k
	}
	return Is[len(Is)-1].Max()
}

// AreEqual returns true if and only if the ranges R and S are equal.
func AreEqual(R Range, S Range) bool {
	// Check that R and S are of the same length (and handle the empty case)
	n := Len(R)
	if Len(S) != n {
		return false
	} else if n == 0 {
		return true
	}
	// If both R and S are intervals, this is easy
	if I, ok := R.(Interval); ok {
		if J, ok := S.(Interval); ok {
			return I.Min() == J.Min()
		}
	}
	// Check that |R| = |S| = |R \cap S|
	return Len(Meet(R, S)) == n
}

// Include returns the range constructed from the range R by including n. That is, it returns the union R \cup {n}.
func Include(R Range, n int64) Range {
	if Contains(R, n) {
		return R
	}
	S := append(R.Intervals(), ToInterval(n, n))
	return ToRange(S...)
}

// Exclude returns the range constructed from the range R by removing n. That is, on success it returns the difference R \setminus {n}. Returns errors.ErrNotInRange if n does not lie in R.
func Exclude(R Range, n int64) (Range, error) {
	// Compute the interval containing n
	S := R.Intervals()
	ok, j := findInterval(S, n)
	if !ok {
		return Empty, errors.ErrNotInRange
	}
	// copy the intervals 0..j-1
	T := make([]Interval, j, len(S)+1)
	copy(T, S)
	// Subdivide the j-th interval [a,b]. Note that if n is an endpoint then at
	// least one of the intervals added will be empty.
	a, b := S[j].Min(), S[j].Max()
	T = append(T, ToInterval(a, n-1), ToInterval(n+1, b))
	// Copy the rest of the intervals
	T = append(T, S[j+1:]...)
	// Convert to a range
	return ToRange(T...), nil
}

// Diff returns the range of elements that lie in the range R but not in S. That is, it returns the difference R \setminus S.
func Diff(R Range, S Range) Range {
	// Handle the trivial case
	if R.IsEmpty() || S.IsEmpty() {
		return R
	}
	// Tautologically R \setminus S is the intersection of R with the complement
	// of S: R \setminus S = R \cap (R \setminus S). Let I be any interval
	// containing R. Then: R \setminus S = R \cap (I \setminus S).
	return Meet(R, ToRange(complement(Span(R), S.Intervals())...))
}

// Meet returns the range of elements that lie in both ranges R and S. That is, it returns the intersection R \cap S.
func Meet(R Range, S Range) Range {
	// Handle the trivial case
	if R.IsEmpty() || S.IsEmpty() {
		return Empty
	}
	// If both R and S are intervals, this is easy
	if I, ok := R.(Interval); ok {
		if J, ok := S.(Interval); ok {
			if ok, meet, _ := overlaps(I, J); ok {
				return meet
			}
			return Empty
		}
	}
	// Prepare the result
	IR, IS := R.Intervals(), S.Intervals()
	T := make([]Interval, 0, len(IR)*len(IS))
	// Compute [I meet J : I in R, J in S]
	for _, I := range IR {
		T = append(T, meetInterval(I, IS)...)
	}
	return ToRange(T...)
}

// Join returns that range of elements that lie in either the range R or S. That is, it returns the union R \cup S.
func Join(R Range, S Range) Range {
	// Handle the trivial case
	if R.IsEmpty() {
		return S
	} else if S.IsEmpty() {
		return R
	}
	// If both R and S are intervals, this is easy
	if I, ok := R.(Interval); ok {
		if J, ok := S.(Interval); ok {
			if ok, _, join := overlaps(I, J); ok {
				return join
			}
			return ToRange(I, J)
		}
	}
	// Take the union of the two sets of intervals
	IR, IS := R.Intervals(), S.Intervals()
	T := make([]Interval, len(IR)+len(IS))
	copy(T, IR)
	copy(T[len(IR):], IS)
	return ToRange(T...)
}

// ToRange converts the given intervals to a Range.
func ToRange(S ...Interval) Range {
	T := sortAndMerge(S)
	if n := len(T); n == 0 {
		return Empty
	} else if n == 1 {
		return T[0]
	}
	return irange(T)
}

// Parse parses s and returns a range. s should be of the form "[A,B,...,C]" where each of A, B, ..., C are either integers that will fit into an int64 or substrings of the form "E..F" where E and F are integers that will fit into an int64.
func Parse(s string) (Range, error) {
	// Remove leading and trailing whitespace
	s = strings.TrimSpace(s)
	// Remove the leading and trailing square brackets
	if len(s) == 0 || s[0] != '[' || s[len(s)-1] != ']' {
		return Empty, errors.ErrParse
	}
	s = strings.TrimSpace(s[1 : len(s)-1])
	// Handle the trivial case
	if len(s) == 0 {
		return Empty, nil
	}
	// Split on commas
	pieces := strings.Split(s, ",")
	S := make([]Interval, 0, len(pieces))
	for _, p := range pieces {
		if I, err := parseInterval(p); err != nil {
			return Empty, err
		} else if !I.IsEmpty() {
			S = append(S, I)
		}
	}
	// Return the range
	return ToRange(S...), nil
}
