// Inmem_test provides tests for the inmem package.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package inmem

import (
	"bitbucket.org/pcas/irange/internal/irangetest"
	"context"
	"testing"
	"time"
)

func TestAll(t *testing.T) {
	// Create the store
	s := New(nil)
	// Create a context with reasonable timeout for the tests to run
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	// Run the tests
	irangetest.Run(ctx, s, t)
}
