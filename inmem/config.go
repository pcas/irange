// Config defines configuration options that can be set for an inmem storage engine

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package inmem

import (
	irerrors "bitbucket.org/pcas/irange/errors"
	"bitbucket.org/pcastools/log"
	"errors"
	"sync"
)

// Config describes the configuration options we allow a user to set on an inmem instance.
type Config struct {
	MaxRetries     int           // the maximum number of times an entry can be retried
	MaxConcurrency int           // the maximum number of simultaneously active entries in a range
	Log            log.Interface // the logger
}

// Default values
const (
	DefaultMaxRetries     = 7
	DefaultMaxConcurrency = 32767
)

// The default values for the client configuration, along with controlling mutex. The initial default values are assigned at init.
var (
	defaultsm sync.Mutex
	defaults  *Config
)

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// init sets the initial default values.
func init() {
	defaults = &Config{
		MaxRetries:     DefaultMaxRetries,
		MaxConcurrency: DefaultMaxConcurrency,
		Log:            log.Discard,
	}
}

/////////////////////////////////////////////////////////////////////////
// Config functions
/////////////////////////////////////////////////////////////////////////

// DefaultConfig returns a new client configuration initialised with the default values.
func DefaultConfig() *Config {
	defaultsm.Lock()
	defer defaultsm.Unlock()
	return defaults.Copy()
}

// SetDefaultConfig sets the default configuration to c and returns the old default configuration. This change will be reflected in future calls to DefaultConfig.
func SetDefaultConfig(c *Config) *Config {
	defaultsm.Lock()
	defer defaultsm.Unlock()
	oldc := defaults
	defaults = c.Copy()
	return oldc
}

// Validate validates the configuration, returning an error if there's a problem.
func (c *Config) Validate() error {
	if c == nil {
		return irerrors.ErrNilConfiguration
	} else if c.MaxRetries < 0 {
		return errors.New("MaxRetries must be non-negative")
	} else if c.MaxConcurrency <= 0 {
		return errors.New("MaxConcurrency must be positive")
	}
	return nil
}

// Copy returns a copy of the configuration.
func (c *Config) Copy() *Config {
	cc := *c
	return &cc
}
