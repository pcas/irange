// Worker defines a worker that maintains the status of a range object.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package inmem

import (
	"bitbucket.org/pcas/irange"
	"bitbucket.org/pcas/irange/errors"
	"bitbucket.org/pcastools/ulid"
	"time"
)

// asEntryErr returns n and err as an asEntryErr object. If err is nil, it also examines errors to find how many times n has failed. It leaves the Deadline unset.
func asEntryErr(n int64, err error, errors []irange.Range) entryErr {
	// handle the error case
	if err != nil {
		return entryErr{err: err}
	}
	// Prepare an ID
	u, err := ulid.New()
	id := irange.ID(u)
	// Calculate the number of failures
	var numFailures int
	for i, R := range errors {
		if irange.Contains(R, n) {
			numFailures = i + 1
		}
	}
	// Build and return the result
	return entryErr{
		Entry: &Entry{
			id:       id,
			value:    n,
			failures: numFailures,
		},
		err: err,
	}
}

// removeFromStack removes the entry with the given ID from the map of active entries and the stack, and returns it.
func removeFromStack(s *status, id irange.ID) (*Info, error) {
	// Retrieve and delete the entry info
	e, ok := s.active[id]
	if !ok {
		return nil, ErrInternal // this can't happen
	}
	delete(s.active, id)
	// Remove the entry from the stack
	if x := s.stack.Remove(e.Deadline(), id); x != id {
		return nil, ErrInternal // this can't happen
	}
	return e, nil
}

// removeFromErrors removes the given entry from the error ranges in s, if present. It returns a nil error.
func removeFromErrors(s *status, e *Info) error {
	var err error
	if i := e.failures; i > 0 {
		if s.errors[i-1], err = irange.Exclude(s.errors[i-1], e.value); err != nil {
			return ErrInternal // this can't happen
		}
	}
	return nil
}

// bumpError increases the error count for entry e, or marks it as failed if it has already reached the maximum number of errors.
func bumpError(s *status, e *Info) error {
	// Did this job fail before? If so remove it from the relevant error range
	if err := removeFromErrors(s, e); err != nil {
		return err // this can't happen
	}
	// Retry the entry, if appropriate
	e.failures++
	if e.failures < len(s.errors) {
		// We can retry this entry
		s.errors[e.failures-1] = irange.Include(s.errors[e.failures-1], e.value)
		s.pending = irange.Include(s.pending, e.value)
	} else {
		// This entry has failed too many times, so mark it as failed
		s.failed = irange.Include(s.failed, e.value)
	}
	return nil
}

// pruneStack increases the failure count for stale entries, and requeues them if appropriate.
func pruneStack(s *status) error {
	for _, x := range s.stack.Prune(time.Now()) {
		u, ok := x.(irange.ID)
		if !ok {
			return ErrInternal // this can't happen
		}
		// Retrieve and delete the entry info
		e, ok := s.active[u]
		if !ok {
			return ErrInternal // this can't happen
		}
		delete(s.active, u)
		// Increase the error count for this entry, or mark it as failed
		if err := bumpError(s, e); err != nil {
			return err // this can't happen
		}
	}
	return nil
}

/////////////////////////////////////////////////////////////////////////
// handler functions
/////////////////////////////////////////////////////////////////////////

// handleRequeue handles a Requeue request for the given ID.
func handleRequeue(s *status, id irange.ID) error {
	// Remove the entry from the stack
	e, err := removeFromStack(s, id)
	if err != nil {
		return err // this can't happen
	}
	// Mark that this value needs processing
	s.pending = irange.Include(s.pending, e.Value())
	return nil
}

// handleSuccess handles a Success request for the given ID.
func handleSuccess(s *status, id irange.ID) error {
	// Remove the entry from the stack
	e, err := removeFromStack(s, id)
	if err != nil {
		return err // this can't happen
	}
	// Did this job fail before? If so remove it from the relevant error range
	if err := removeFromErrors(s, e); err != nil {
		return err // this can't happen
	}
	// Mark this entry as succeeded
	s.succeeded = irange.Include(s.succeeded, e.Value())
	return nil
}

// handleError handles a Error request for the given ID.
func handleError(s *status, id irange.ID) error {
	// Remove the entry from the stack
	e, err := removeFromStack(s, id)
	if err != nil {
		return err // this can't happen
	}
	// Increase the error count for e or mark it as failed
	if err := bumpError(s, e); err != nil {
		return err // this can't happen
	}
	return nil
}

// handleFatal handles a Fatal request for the given ID.
func handleFatal(s *status, id irange.ID) error {
	// Remove the entry from the stack
	e, err := removeFromStack(s, id)
	if err != nil {
		return err // this can't happen
	}
	// Did this job fail before? If so remove it from the relevant error range
	if err := removeFromErrors(s, e); err != nil {
		return err // this can't happen
	}
	// Mark this entry as failed
	s.failed = irange.Include(s.failed, e.Value())
	return nil
}

// handleInfo handles an Info request for the value n.
func handleInfo(s *status, n int64) (*Info, error) {
	// Search the active range
	for _, e := range s.active {
		if e.Value() == n {
			return e, nil
		}
	}
	// Did this job succeed already?
	if irange.Contains(s.succeeded, n) {
		return &Info{value: n, state: irange.Succeeded}, nil
	}
	// Did it fail?
	if irange.Contains(s.failed, n) {
		return &Info{value: n, state: irange.Failed}, nil
	}
	// Search the pending range
	if irange.Contains(s.pending, n) {
		// Count the number of failures
		for i, R := range s.errors {
			if irange.Contains(R, n) {
				return &Info{value: n, state: irange.Pending, failures: i + 1}, nil
			}
		}
		// There were no failures
		return &Info{value: n, state: irange.Pending, failures: 0}, nil
	}
	// This value isn't in any of the ranges
	return nil, errors.ErrNotInRange
}

// handleSetDeadline handles a SetDeadline request for the entry e with given ID.
func handleSetDeadline(s *status, id irange.ID, e *Info) error {
	// Put the ID for e onto the stack, with associated time e.Deadline()
	if x := s.stack.Insert(e.Deadline(), id); x != nil {
		return ErrInternal // this can't happen
	}
	// Add e to the map of active entries
	s.active[id] = e
	// Record that this ID is associated with this range
	s.c.Lock()
	defer s.c.Unlock()
	s.c.nameFromID[id] = s.name
	return nil
}

// handleRequeueActive handles a RequeueActive request.
func handleRequeueActive(s *status) error {
	// Grab the IDs of all active jobs
	ids := make([]irange.ID, 0, len(s.active))
	for x := range s.active {
		ids = append(ids, x)
	}
	// Requeue each job in turn
	for _, id := range ids {
		if err := handleRequeue(s, id); err != nil {
			return err
		}
	}
	return nil
}

// handleRequeueSucceeded handles a RequeueSucceeded request.
func handleRequeueSucceeded(s *status) error {
	for _, id := range irange.Contents(s.succeeded) {
		s.pending = irange.Include(s.pending, id)
	}
	s.succeeded = irange.Empty
	return nil
}

// handleRequeueFailed handles a RequeueFailed request.
func handleRequeueFailed(s *status) error {
	for _, id := range irange.Contents(s.failed) {
		s.pending = irange.Include(s.pending, id)
	}
	s.failed = irange.Empty
	for i := range s.errors {
		s.errors[i] = irange.Empty
	}
	return nil
}

// respondToRequest processes the request req and returns an appropriate response object.
func respondToRequest(s *status, req *request) *response {
	var err error
	resp := &response{
		pending:   irange.Empty,
		active:    irange.Empty,
		succeeded: irange.Empty,
		failed:    irange.Empty,
	}
	// we handle requests that affect the state before we check for expired entries
	switch req.code {
	case RequeueRequest:
		err = handleRequeue(s, req.id)
	case SuccessRequest:
		err = handleSuccess(s, req.id)
	case ErrorRequest:
		err = handleError(s, req.id)
	case FatalRequest:
		err = handleFatal(s, req.id)
	case SetDeadlineRequest:
		err = handleSetDeadline(s, req.id, req.info)
	case RequeueActiveRequest:
		err = handleRequeueActive(s)
	case RequeueSucceededRequest:
		err = handleRequeueSucceeded(s)
	case RequeueFailedRequest:
		err = handleRequeueFailed(s)
	case StatusRequest, InfoRequest:
		// continue
	default:
		err = ErrInternal // this can't happen
	}
	// handle any error.
	if err != nil {
		return errResponse(err)
	}
	// check for expired entries
	if err := pruneStack(s); err != nil {
		return errResponse(err) // this can't happen
	}
	// now we handle requests that report status
	switch req.code {
	case StatusRequest:
		resp.pending = s.pending
		resp.succeeded = s.succeeded
		resp.failed = s.failed
		// build the active entries as a Range
		for _, e := range s.active {
			resp.active = irange.Include(resp.active, e.Value())
		}
	case InfoRequest:
		resp.info, err = handleInfo(s, req.n)
		if err != nil {
			return errResponse(err)
		}
	default:
		// continue
	}
	return resp
}

/////////////////////////////////////////////////////////////////////////
// the worker
/////////////////////////////////////////////////////////////////////////

// statusWorker is the worker that manages the status s. It listens for requests down the channel s.requestC and passes responses down s.responseC.
func statusWorker(s *status) {
mainLoop:
	for {
		n, R, err := irange.Compact.Next(s.pending)
		if len(s.active) >= s.maxConcurrency || (err == errors.ErrEmpty && len(s.active) != 0) {
			// we wait for either an active entry to complete or for shutdown
			select {
			case req := <-s.requestC:
				// process the request
				s.responseC <- respondToRequest(s, req)
				// and continue
			case <-s.doneC:
				break mainLoop // we shut down
			}
		} else {
			// wrap up n, or any error
			e := asEntryErr(n, err, s.errors)
			// try to push e down the output channel
			select {
			case s.C <- e:
				// if we just sent an error, we continue. Otherwise:
				if e.err == nil {
					// update the range
					s.pending = R
					// read and process the SetDeadline request
					req := <-s.requestC
					s.responseC <- respondToRequest(s, req)
				}
			case req := <-s.requestC:
				// process the request
				s.responseC <- respondToRequest(s, req)
				// and continue
			case <-s.doneC:
				break mainLoop // we shut down
			}
		}
	}
	// close the channels and exit
	close(s.C)
	close(s.requestC)
	close(s.responseC)
	return
}
