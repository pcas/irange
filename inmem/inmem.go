// Inmem is an in-memory storage engine for range objects.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package inmem

import (
	"bitbucket.org/pcas/irange"
	irerrors "bitbucket.org/pcas/irange/errors"
	"bitbucket.org/pcastools/log"
	"bitbucket.org/pcastools/timeutil"
	"bitbucket.org/pcastools/ulid"
	"context"
	"errors"
	"strings"
	"sync"
	"time"
)

// status represents the current status of a range.
type status struct {
	c              *client       // The storage engine for this range
	name           string        // The name for this range
	lifetime       time.Duration // the lifetime of entries for this range
	maxConcurrency int           // the maximum number of simultaneously active entries in this range
	initial        irange.Range  // the initial range of entries

	// Channels for communicating with the statusWorker responsible for the status.
	C         chan entryErr  // C is the output channel
	requestC  chan *request  // requestC is a channel for requests.
	responseC chan *response // responseC is a channel for responses
	doneC     chan struct{}  // doneC should be closed to shut down the statusWorker

	//  The following values should only be updated by the statusWorker responsible for the status.
	pending   irange.Range        // the pending entries
	stack     *timeutil.Stack     // the stack of IDs of active entries, ordered by Deadline
	active    map[irange.ID]*Info // the active entries
	succeeded irange.Range        // the range of entries that have succeeded
	errors    []irange.Range      // errors[i-1] holds the range of entries that have failed i times
	failed    irange.Range        // the range of entries that have failed
}

// requestCode represents a request to the statusWorker
type requestCode uint8

// request codes
const (
	SuccessRequest = requestCode(iota)
	ErrorRequest
	RequeueRequest
	FatalRequest
	StatusRequest
	InfoRequest
	SetDeadlineRequest
	RequeueActiveRequest
	RequeueSucceededRequest
	RequeueFailedRequest
)

// entryErr is an entry in a range together with an error
type entryErr struct {
	*Entry
	err error
}

// request specifies a request to the statusWorker.
type request struct {
	code requestCode // the code specifying the type of request
	id   irange.ID   // the entry ID, for Success, Error, Requeue, Fatal, and SetDeadline requests. irange.NilID otherwise.
	n    int64       // the entry, for Info requests. Zero otherwise.
	info *Info       // the info object, for SetDeadline requests. nil otherwise
}

// response describes a response to a request.
type response struct {
	err       error        // the error caused by the request
	info      *Info        // the *Info response to an Info request
	pending   irange.Range // the pending range, for a Status request
	active    irange.Range // the active range, for a Status request
	succeeded irange.Range // the succeeded range, for a Status request
	failed    irange.Range // the failed range, for a Status request
}

// client is an in-memory storage engine for irange.Range objects
type client struct {
	sync.RWMutex
	log.BasicLogable
	// nameFromID records the name of the range associated to an entry ID
	nameFromID map[irange.ID]string
	// ranges records the status of the range associated to a name
	ranges map[string]*status
	// maxRetries specifies the maximum number of times that an entry may be retried
	maxRetries int
	// maxConcurrency specifies the maximum number of simultaneously active entries in a range
	maxConcurrency int
}

// errors
var (
	ErrNilClient = errors.New("uninitialised client")
	ErrInternal  = errors.New("fatal internal error")
)

// Assert that the client satisfies the irange.Storage interface
var _ irange.Storage = &client{}

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// validateName validates the range name s. Range names that are empty or start or end with whitespace are invalid.
func validateName(s string) error {
	if t := strings.TrimSpace(s); s == "" || t != s {
		return irerrors.ErrInvalidName
	}
	return nil
}

// validateCreate validates the arguments to Create
func validateCreate(c *client, name string, maxRetries int, maxConcurrency int) error {
	if c == nil {
		return ErrNilClient
	} else if err := validateName(name); err != nil {
		return err
	} else if maxRetries < 0 {
		return errors.New("maxRetries must be non-negative")
	} else if maxConcurrency <= 0 {
		return errors.New("maxConcurrency must be positive")
	}
	return nil
}

// errReponse wraps err in a *response
func errResponse(err error) *response {
	return &response{err: err}
}

// compare returns an integer comparing x and y lexicographically. The result will be 0 if u == v, -1 if u < v, and +1 if u > v.
func compare(x irange.ID, y irange.ID) int {
	return ulid.Compare(ulid.ULID(x), ulid.ULID(y))
}

// makeRangeSlice returns a slice of empty ranges of given length.
func makeRangeSlice(n int) []irange.Range {
	S := make([]irange.Range, n)
	for i := 0; i < n; i++ {
		S[i] = irange.Empty
	}
	return S
}

/////////////////////////////////////////////////////////////////////////
// client functions
/////////////////////////////////////////////////////////////////////////

// New returns a new inmem storage engine with the given configuration. If cfg is nil, the default configuration is used.
func New(cfg *Config) irange.Storage {
	if cfg == nil {
		cfg = DefaultConfig()
	}
	// Build the client
	c := &client{
		nameFromID:     map[irange.ID]string{},
		ranges:         map[string]*status{},
		maxRetries:     cfg.MaxRetries,
		maxConcurrency: cfg.MaxConcurrency,
	}
	// set the logger and return
	c.SetLogger(cfg.Log)
	return c
}

// statusFromID returns the status object associated to the given entry ID.
func (c *client) statusFromID(id irange.ID) (*status, error) {
	// Get a read lock on the client
	c.RLock()
	defer c.RUnlock()
	// Look up the ID
	name, ok := c.nameFromID[id]
	if !ok {
		return nil, irerrors.ErrUnknownID
	}
	// Look up the status
	st, ok := c.ranges[name]
	if !ok {
		return nil, irerrors.ErrUnknownRange
	}
	return st, nil
}

// statusFromName returns the status object with the given name
func (c *client) statusFromName(name string) (*status, error) {
	// Get a read lock on the client
	c.RLock()
	defer c.RUnlock()
	// Look up the status
	st, ok := c.ranges[name]
	if !ok {
		return nil, irerrors.ErrUnknownRange
	}
	return st, nil
}

// Create creates a range r with given name. Entries have the given lifetime; they are retried at most maxRetries times on failure, and at most maxConcurrency entries can be active at the same time.
func (c *client) Create(_ context.Context, name string, r irange.Range, lifetime time.Duration, maxRetries int, maxConcurrency int) error {
	// Sanity checks
	if err := validateCreate(c, name, maxRetries, maxConcurrency); err != nil {
		return err
	}
	// Make a logger
	lg := log.PrefixWith(c.Log(), "[Create]")
	// Check that maxRetries and maxConcurrency are not too large
	if maxRetries > c.maxRetries {
		lg.Printf("resetting maxRetries (%d) to the maximum for this storage engine (%d)", maxRetries, c.maxRetries)
		maxRetries = c.maxRetries
	}
	if maxConcurrency > c.maxConcurrency {
		lg.Printf("resetting maxConcurrency (%d) to the maximum for this storage engine (%d)", maxConcurrency, c.maxConcurrency)
		maxConcurrency = c.maxConcurrency
	}
	// Make the new range status
	st := &status{
		c:              c,
		name:           name,
		initial:        r,
		pending:        r,
		active:         make(map[irange.ID]*Info),
		stack:          timeutil.NewStack(func(x interface{}, y interface{}) bool { return compare(x.(irange.ID), y.(irange.ID)) == -1 }),
		succeeded:      irange.Empty,
		errors:         makeRangeSlice(maxRetries),
		failed:         irange.Empty,
		lifetime:       lifetime,
		maxConcurrency: maxConcurrency,
		C:              make(chan entryErr),
		requestC:       make(chan *request),
		responseC:      make(chan *response),
		doneC:          make(chan struct{}),
	}
	// Launch the worker and store the range
	go statusWorker(st)
	c.Lock()
	defer c.Unlock()
	if _, ok := c.ranges[name]; ok {
		return irerrors.ErrRangeExists
	}
	c.ranges[name] = st
	return nil
}

// Delete deletes the range with the given name
func (c *client) Delete(_ context.Context, name string) error {
	// Sanity checks
	if c == nil {
		return ErrNilClient
	} else if err := validateName(name); err != nil {
		return err
	}
	// Lock the client
	c.Lock()
	defer c.Unlock()
	// Try to grab the range
	st, ok := c.ranges[name]
	if !ok {
		return irerrors.ErrUnknownRange
	}
	// shut down the worker
	close(st.doneC)
	// delete the range
	delete(c.ranges, name)
	return nil
}

// RequeueActive requeues all active entries in the range with the given name
func (c *client) RequeueActive(ctx context.Context, name string) error {
	// Sanity checks
	if c == nil {
		return ErrNilClient
	} else if err := validateName(name); err != nil {
		return err
	}
	// Retrieve the status
	st, err := c.statusFromName(name)
	if err != nil {
		return err
	}
	// Make the request
	select {
	case st.requestC <- &request{code: RequeueActiveRequest}:
		// the request was made
	case <-ctx.Done():
		// our context says we should abort
		return ctx.Err()
	}
	// Read the response
	resp := <-st.responseC
	return resp.err
}

// RequeueSucceeded requeues all entries that have succeeded in the range with the given name
func (c *client) RequeueSucceeded(ctx context.Context, name string) error {
	// Sanity checks
	if c == nil {
		return ErrNilClient
	} else if err := validateName(name); err != nil {
		return err
	}
	// Retrieve the status
	st, err := c.statusFromName(name)
	if err != nil {
		return err
	}
	// Make the request
	select {
	case st.requestC <- &request{code: RequeueSucceededRequest}:
		// the request was made
	case <-ctx.Done():
		// our context says we should abort
		return ctx.Err()
	}
	// Read the response
	resp := <-st.responseC
	return resp.err
}

// RequeueFailed requeues all failed entries in the range with the given name
func (c *client) RequeueFailed(ctx context.Context, name string) error {
	// Sanity checks
	if c == nil {
		return ErrNilClient
	} else if err := validateName(name); err != nil {
		return err
	}
	// Retrieve the status
	st, err := c.statusFromName(name)
	if err != nil {
		return err
	}
	// Make the request
	select {
	case st.requestC <- &request{code: RequeueFailedRequest}:
		// the request was made
	case <-ctx.Done():
		// our context says we should abort
		return ctx.Err()
	}
	// Read the response
	resp := <-st.responseC
	return resp.err
}

// Next returns the next entry in the range with the given name, or errors.ErrEmpty if no such entry exists.  The caller identifies itself to c via m.
func (c *client) Next(ctx context.Context, name string, m *irange.Metadata) (irange.Entry, error) {
	// Sanity checks
	if c == nil {
		return nil, ErrNilClient
	} else if err := validateName(name); err != nil {
		return nil, err
	}
	// Retrieve the status
	st, err := c.statusFromName(name)
	if err != nil {
		return nil, err
	}
	// Read from the status channel, or time out
	var e entryErr
	select {
	case e = <-st.C:
		// continue
	case <-ctx.Done():
		// our context says we should abort
		return nil, ctx.Err()
	}
	// Check the error
	if e.err != nil {
		return nil, e.err
	}
	// Package the entry as a *Info and pass it back to the worker
	now := time.Now()
	x := &Info{
		value:    e.Value(),
		state:    irange.Active,
		metadata: m,
		start:    now,
		deadline: now.Add(st.lifetime),
		failures: e.Failures(),
	}
	st.requestC <- &request{code: SetDeadlineRequest, info: x, id: e.ID()}
	// Read the response and check if there was an error
	resp := <-st.responseC
	if err := resp.err; err != nil {
		return nil, err
	}
	// Return the entry
	return &Entry{id: e.ID(), value: x.Value(), deadline: x.Deadline(), failures: x.Failures()}, nil
}

// Success indicates that the entry with the given ID has succeeded.
func (c *client) Success(ctx context.Context, id irange.ID) error {
	// Sanity check
	if c == nil {
		return ErrNilClient
	}
	// Retrieve the status
	st, err := c.statusFromID(id)
	if err != nil {
		return err
	}
	// Make the request
	select {
	case st.requestC <- &request{code: SuccessRequest, id: id}:
		// the request was made
	case <-ctx.Done():
		// our context says we should abort
		return ctx.Err()
	}
	// Read the response
	resp := <-st.responseC
	return resp.err
}

// Error indicates that the entry with the given ID has failed and should be retried.
func (c *client) Error(ctx context.Context, id irange.ID) error {
	// Sanity check
	if c == nil {
		return ErrNilClient
	}
	// Retrieve the status
	st, err := c.statusFromID(id)
	if err != nil {
		return err
	}
	// Make the request
	select {
	case st.requestC <- &request{code: ErrorRequest, id: id}:
		// the request was made
	case <-ctx.Done():
		// our context says we should abort
		return ctx.Err()
	}
	// Read the response
	resp := <-st.responseC
	return resp.err
}

// Requeue indicates that the entry with the given ID should be requeued, without incrementing the number of failures.
func (c *client) Requeue(ctx context.Context, id irange.ID) error {
	// Sanity check
	if c == nil {
		return ErrNilClient
	}
	// Retrieve the status
	st, err := c.statusFromID(id)
	if err != nil {
		return err
	}
	// Make the request
	select {
	case st.requestC <- &request{code: RequeueRequest, id: id}:
		// the request was made
	case <-ctx.Done():
		// our context says we should abort
		return ctx.Err()
	}
	// Read the response
	resp := <-st.responseC
	return resp.err
}

// Fatal indicates that the entry with the given ID has failed and should not be requeued.
func (c *client) Fatal(ctx context.Context, id irange.ID) error {
	// Sanity check
	if c == nil {
		return ErrNilClient
	}
	// Retrieve the status
	st, err := c.statusFromID(id)
	if err != nil {
		return err
	}
	// Make the request
	select {
	case st.requestC <- &request{code: FatalRequest, id: id}:
		// the request was made
	case <-ctx.Done():
		// our context says we should abort
		return ctx.Err()
	}
	// Read the response
	resp := <-st.responseC
	return resp.err
}

// List returns the names of all known ranges.
func (c *client) List(_ context.Context) ([]string, error) {
	// Sanity check
	if c == nil {
		return nil, ErrNilClient
	}
	// Lock the client
	c.RLock()
	defer c.RUnlock()
	// Build the result
	result := make([]string, 0, len(c.ranges))
	for k := range c.ranges {
		result = append(result, k)
	}
	return result, nil
}

// Status returns the status of the range with the given name.
func (c *client) Status(ctx context.Context, name string) (irange.Status, error) {
	// Sanity checks
	if c == nil {
		return nil, ErrNilClient
	} else if err := validateName(name); err != nil {
		return nil, err
	}
	// Retrieve the status
	st, err := c.statusFromName(name)
	if err != nil {
		return nil, err
	}
	// Make the request
	select {
	case st.requestC <- &request{code: StatusRequest}:
		// the request was made
	case <-ctx.Done():
		// our context says we should abort
		return nil, ctx.Err()
	}
	// Read the response
	resp := <-st.responseC
	// Check for any error
	if resp.err != nil {
		return nil, resp.err
	}
	// Return the response
	return &Status{
		pending:   resp.pending,
		active:    resp.active,
		succeeded: resp.succeeded,
		failed:    resp.failed,
	}, nil
}

// Info returns information about the given entry in the range with the given name.
func (c *client) Info(ctx context.Context, name string, entry int64) (irange.Info, error) {
	// Sanity checks
	if c == nil {
		return nil, ErrNilClient
	} else if err := validateName(name); err != nil {
		return nil, err
	}
	// Retrieve the status
	st, err := c.statusFromName(name)
	if err != nil {
		return nil, err
	}
	// Make the request
	select {
	case st.requestC <- &request{code: InfoRequest, n: entry}:
		// the request was made
	case <-ctx.Done():
		// our context says we should abort
		return nil, ctx.Err()
	}
	// Read and return the response
	resp := <-st.responseC
	return resp.info, resp.err
}
