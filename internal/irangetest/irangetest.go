// Irangetest provides test functions for implementations of irange.Storage.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package irangetest

import (
	"bitbucket.org/pcas/irange"
	"bitbucket.org/pcas/irange/errors"
	"bitbucket.org/pcastools/ulid"
	"context"
	"github.com/stretchr/testify/require"
	"os"
	"testing"
	"time"
)

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// wrap wraps a test function so that it can be passed to testing.T.Run.
func wrap(ctx context.Context, s irange.Storage, f func(context.Context, irange.Storage, *require.Assertions)) func(*testing.T) {
	return func(t *testing.T) {
		f(ctx, s, require.New(t))
	}
}

// getFailures returns the number of times that n has failed in the range with the given name on s.
func getFailures(ctx context.Context, s irange.Storage, name string, n int64) (int, error) {
	x, err := s.Info(ctx, name, n)
	if err != nil {
		return 0, err
	}
	return x.Failures(), nil
}

// statesAndFailures returns parallel slices of the states and numbers of failures of the entries in E for the range with the given name on s.
func statesAndFailures(ctx context.Context, s irange.Storage, name string, E []int64) ([]irange.State, []int, error) {
	states := make([]irange.State, 0, len(E))
	failures := make([]int, 0, len(E))
	for _, n := range E {
		x, err := s.Info(ctx, name, n)
		if err != nil {
			return nil, nil, err
		}
		states = append(states, x.State())
		failures = append(failures, x.Failures())
	}
	return states, failures, nil
}

// makeMetadata returns a metadata object with the client application name set to s and the hostname set to os.Hostname().
func makeMetadata(s string) *irange.Metadata {
	hostname, err := os.Hostname()
	if err != nil {
		hostname = "unknown"
	}
	return &irange.Metadata{
		AppName:  s,
		Hostname: hostname,
	}
}

/////////////////////////////////////////////////////////////////////////
// Test functions
/////////////////////////////////////////////////////////////////////////

// testInvalidName tests that invalid names are handled correctly.
func testInvalidName(ctx context.Context, s irange.Storage, require *require.Assertions) {
	names := []string{
		"",
		" invalid",
		"invalid ",
	}
	for _, name := range names {
		// test Create
		err := s.Create(ctx, name, irange.Empty, time.Second, 1, 1)
		require.Error(err)
		require.Equal(errors.ErrInvalidName, err)
		// test Delete
		err = s.Delete(ctx, name)
		require.Error(err)
		require.Equal(errors.ErrInvalidName, err)
		// test Next
		_, err = s.Next(ctx, name, makeMetadata("testInvalidName"))
		require.Error(err)
		require.Equal(errors.ErrInvalidName, err)
		// test Status
		_, err = s.Status(ctx, name)
		require.Error(err)
		require.Equal(errors.ErrInvalidName, err)
		// test Info
		_, err = s.Info(ctx, name, 0)
		require.Error(err)
		require.Equal(errors.ErrInvalidName, err)
	}
}

// testEmpty tests that empty ranges are handled correctly.
func testEmpty(ctx context.Context, s irange.Storage, require *require.Assertions) {
	// Create an empty range
	err := s.Create(ctx, "empty", irange.Empty, time.Second, 1, 1)
	require.NoError(err)
	// Iterate over an empty range
	_, err = s.Next(ctx, "empty", makeMetadata("testEmpty"))
	require.Error(err)
	require.Equal(errors.ErrEmpty, err)
	// Get the Status of an empty range
	st, err := s.Status(ctx, "empty")
	require.NoError(err)
	require.Zero(irange.Len(st.Pending()))
	require.Zero(irange.Len(st.Active()))
	require.Zero(irange.Len(st.Succeeded()))
	require.Zero(irange.Len(st.Failed()))
	// Get Info about an empty range
	_, err = s.Info(ctx, "empty", 77)
	require.Error(err)
	// Delete the range
	err = s.Delete(ctx, "empty")
	require.NoError(err)
}

// testIteration tests Success, Fatal, Requeue, and Error.
func testIteration(ctx context.Context, s irange.Storage, require *require.Assertions) {
	// Build a Range to work with
	err := s.Create(ctx, "myRange", irange.ToInterval(1, 5), 100*time.Millisecond, 3, 5)
	require.NoError(err)
	// Loop over the range
	var e irange.Entry
	var requeuedAlready bool
	var numFailures int
	for {
		e, err = s.Next(ctx, "myRange", makeMetadata("testIteration"))
		if err != nil {
			// iteration has finished
			require.Equal(errors.ErrEmpty, err)
			break
		}
		// make a fatal error for 2, a recoverable error for 3, requeue 4 exactly once, and succeed otherwise
		switch e.Value() {
		case 2:
			s.Fatal(ctx, e.ID())
		case 3:
			n, err := getFailures(ctx, s, "myRange", 3)
			require.NoError(err)
			require.Equal(numFailures, n)
			s.Error(ctx, e.ID())
			numFailures++
		case 4:
			if requeuedAlready {
				s.Success(ctx, e.ID())
			} else {
				requeuedAlready = true
				s.Requeue(ctx, e.ID())
			}
		default:
			s.Success(ctx, e.ID())
		}
	}
	// Check the results
	st, err := s.Status(ctx, "myRange")
	require.NoError(err)
	require.Empty(irange.Contents(st.Pending()))
	require.Empty(irange.Contents(st.Active()))
	require.ElementsMatch([]int64{1, 4, 5}, irange.Contents(st.Succeeded()))
	require.ElementsMatch([]int64{2, 3}, irange.Contents(st.Failed()))
	// Delete the range
	require.NoError(s.Delete(ctx, "myRange"))
}

// testBadIDs tests that incorrect IDs are handled correctly
func testBadIDs(ctx context.Context, s irange.Storage, require *require.Assertions) {
	// Make and store a range
	require.NoError(s.Create(ctx, "myRange", irange.ToInterval(1, 5), 100*time.Millisecond, 3, 5))
	// Grab an entry
	_, err := s.Next(ctx, "myRange", makeMetadata("testIteration"))
	require.NoError(err)
	// Make a random ID
	u, err := ulid.New()
	require.NoError(err)
	id := irange.ID(u)
	// Feed the random ID to Error, Fatal, Requeue, and Success
	require.Equal(errors.ErrUnknownID, s.Error(ctx, id))
	require.Equal(errors.ErrUnknownID, s.Fatal(ctx, id))
	require.Equal(errors.ErrUnknownID, s.Requeue(ctx, id))
	require.Equal(errors.ErrUnknownID, s.Success(ctx, id))
	// Delete the range
	require.NoError(s.Delete(ctx, "myRange"))
}

// testCreateListDelete tests Create, List, and Delete
func testCreateListDelete(ctx context.Context, s irange.Storage, require *require.Assertions) {
	// At the start of the test, there should be no known ranges
	Rs, err := s.List(ctx)
	require.NoError(err)
	require.Empty(Rs)
	// Creating a range that already exists should fail
	require.NoError(s.Create(ctx, "empty1", irange.Empty, 1*time.Second, 1, 1))
	require.Equal(errors.ErrRangeExists, s.Create(ctx, "empty1", irange.Empty, 1*time.Second, 1, 1))
	// We create several more ranges
	require.NoError(s.Create(ctx, "empty2", irange.Empty, 1*time.Second, 1, 1))
	require.NoError(s.Create(ctx, "interval1", irange.ToInterval(3, 5), 1*time.Second, 1, 1))
	require.NoError(s.Create(ctx, "interval2", irange.ToInterval(-7, 11), 1*time.Second, 1, 1))
	// Test List
	names := []string{"empty1", "empty2", "interval1", "interval2"}
	Rs, err = s.List(ctx)
	require.NoError(err)
	require.ElementsMatch(names, Rs)
	// Test Delete
	for _, x := range names {
		require.NoError(s.Delete(ctx, x))
	}
	// Now there should be no known ranges again
	Rs, err = s.List(ctx)
	require.NoError(err)
	require.Empty(Rs)
}

// testUnknownRanges tests that unknown ranges are handled correctly
func testUnknownRanges(ctx context.Context, s irange.Storage, require *require.Assertions) {
	// We make a random ULID, to use as a range name
	u, err := ulid.New()
	require.NoError(err)
	name := u.String()
	// Test Delete, Info, Next, and Status
	require.Equal(errors.ErrUnknownRange, s.Delete(ctx, name))
	_, err = s.Info(ctx, name, 7)
	require.Equal(errors.ErrUnknownRange, err)
	_, err = s.Next(ctx, name, makeMetadata("testUnknownRanges"))
	require.Equal(errors.ErrUnknownRange, err)
	_, err = s.Status(ctx, name)
	require.Equal(errors.ErrUnknownRange, err)
}

// testTimeout tests the timeout logic
func testTimeout(ctx context.Context, s irange.Storage, require *require.Assertions) {
	// Create and store a range
	R := irange.ToInterval(3, 5)
	values := irange.Contents(R)
	require.NoError(s.Create(ctx, "myRange", R, 50*time.Millisecond, 3, 3))
	// Make some metadata
	mt := makeMetadata("testTimeout")
	// Everything should start as pending
	states, failures, err := statesAndFailures(ctx, s, "myRange", values)
	require.NoError(err)
	require.Equal([]irange.State{irange.Pending, irange.Pending, irange.Pending}, states)
	require.Equal([]int{0, 0, 0}, failures)
	// Grab the first entry
	e, err := s.Next(ctx, "myRange", mt)
	require.NoError(err)
	// Now entry 3 should be active
	states, failures, err = statesAndFailures(ctx, s, "myRange", values)
	require.NoError(err)
	require.Equal([]irange.State{irange.Active, irange.Pending, irange.Pending}, states)
	require.Equal([]int{0, 0, 0}, failures)
	// Record this as a success
	require.NoError(s.Success(ctx, e.ID()))
	states, failures, err = statesAndFailures(ctx, s, "myRange", values)
	require.NoError(err)
	require.Equal([]irange.State{irange.Succeeded, irange.Pending, irange.Pending}, states)
	require.Equal([]int{0, 0, 0}, failures)
	// Grab the next entry
	e, err = s.Next(ctx, "myRange", mt)
	require.NoError(err)
	// Now entry 4 should be active
	states, failures, err = statesAndFailures(ctx, s, "myRange", values)
	require.NoError(err)
	require.Equal([]irange.State{irange.Succeeded, irange.Active, irange.Pending}, states)
	require.Equal([]int{0, 0, 0}, failures)
	// Wait for timeout
	time.Sleep(100 * time.Millisecond)
	// Now entry 4 should be pending and have failed
	states, failures, err = statesAndFailures(ctx, s, "myRange", values)
	require.NoError(err)
	require.Equal([]irange.State{irange.Succeeded, irange.Pending, irange.Pending}, states)
	require.Equal([]int{0, 1, 0}, failures)
	// Grab entry 4 again
	e, err = s.Next(ctx, "myRange", mt)
	require.NoError(err)
	require.Equal(e.Value(), int64(4))
	// Record this as a failure
	require.NoError(s.Fatal(ctx, e.ID()))
	states, failures, err = statesAndFailures(ctx, s, "myRange", values)
	require.NoError(err)
	require.Equal([]irange.State{irange.Succeeded, irange.Failed, irange.Pending}, states)
	require.Equal([]int{0, 0, 0}, failures)
	// Grab the last entry
	_, err = s.Next(ctx, "myRange", mt)
	require.NoError(err)
	// Now try to grab another entry with a very short deadline. The timeout will fire.
	shortCtx, cancel := context.WithTimeout(ctx, 1*time.Millisecond)
	defer cancel()
	_, err = s.Next(shortCtx, "myRange", mt)
	require.Equal(context.DeadlineExceeded, err)
	// Nonetheless, we still have active entries
	st, err := s.Status(ctx, "myRange")
	require.Equal([]int64{5}, irange.Contents(st.Active()))
	// Delete the range
	require.NoError(s.Delete(ctx, "myRange"))
}

// testCancellation tests that Next can be cancelled by its context.
func testCancellation(ctx context.Context, s irange.Storage, require *require.Assertions) {
	// Create and store a range with one element
	R := irange.ToInterval(1, 1)
	require.NoError(s.Create(ctx, "myRange", R, 5*time.Second, 3, 3))
	// Make some metadata
	mt := makeMetadata("testTimeout")
	// Grab the first entry
	_, err := s.Next(ctx, "myRange", mt)
	require.NoError(err)
	// Make a context to cancel the next request
	shortCtx, cancel := context.WithCancel(ctx)
	// Request another element, and cancel the request
	go func() {
		time.Sleep(100 * time.Millisecond)
		cancel()
	}()
	_, err = s.Next(shortCtx, "myRange", mt)
	require.Equal(context.Canceled, err)
	// Do the same thing, but cancel using a deadline
	deadlineCtx, cancel := context.WithDeadline(ctx, time.Now().Add(100*time.Millisecond))
	_, err = s.Next(deadlineCtx, "myRange", mt)
	require.Equal(context.DeadlineExceeded, err)
	cancel()
	// Delete the range
	require.NoError(s.Delete(ctx, "myRange"))
}

// testEquality tests IsEqualTo and IsEmpty
func testEquality(ctx context.Context, s irange.Storage, require *require.Assertions) {
	// build some empty ranges
	empty, err := irange.Parse("[]")
	require.NoError(err)
	emptyRanges := []irange.Range{irange.Empty, irange.ToInterval(7, -63), empty}
	// build some non-empty ranges, all distinct
	R, err := irange.Parse("[5..12,93..97]")
	require.NoError(err)
	S, err := irange.Parse("[99,101..1999]")
	require.NoError(err)
	nonemptyRanges := []irange.Range{irange.ToInterval(-63, 7), R, S}
	// test equality of empty ranges
	for _, R := range emptyRanges {
		require.True(irange.AreEqual(empty, R))
		require.True(irange.AreEqual(R, empty))
	}
	// test equality of non-empty ranges
	for i, R := range nonemptyRanges {
		for j, S := range nonemptyRanges {
			require.Equal(i == j, irange.AreEqual(R, S))
			require.Equal(i == j, irange.AreEqual(S, R))
		}
	}
	// test inequality of empty and non-empty ranges
	for _, R := range nonemptyRanges {
		for _, S := range emptyRanges {
			require.False(irange.AreEqual(R, S))
			require.False(irange.AreEqual(S, R))
		}
	}
	// test IsEmpty
	for _, R := range emptyRanges {
		require.True(R.IsEmpty())
	}
	for _, S := range nonemptyRanges {
		require.False(S.IsEmpty())
	}
}

// testRequeue tests RequeueActive, RequeueSucceeded, and RequeueFailed
func testRequeue(ctx context.Context, s irange.Storage, require *require.Assertions) {
	// Create and store a range
	R := irange.ToInterval(1, 6)
	values := irange.Contents(R)
	require.NoError(s.Create(ctx, "myRange", R, 50*time.Millisecond, 3, 3))
	// Make some metadata
	mt := makeMetadata("testRequeue")
	// Everything should start as pending
	states, _, err := statesAndFailures(ctx, s, "myRange", values)
	require.NoError(err)
	expected := []irange.State{
		irange.Pending,
		irange.Pending,
		irange.Pending,
		irange.Pending,
		irange.Pending,
		irange.Pending,
	}
	require.Equal(expected, states)
	// Make the first two entries active
	for i := 0; i < 2; i++ {
		_, err := s.Next(ctx, "myRange", mt)
		require.NoError(err)
	}
	// Grab the next two entries and declare that they succeeded
	for i := 0; i < 2; i++ {
		e, err := s.Next(ctx, "myRange", mt)
		require.NoError(err)
		require.NoError(s.Success(ctx, e.ID()))
	}
	// Grab the next two entries and declare that they failed
	for i := 0; i < 2; i++ {
		e, err := s.Next(ctx, "myRange", mt)
		require.NoError(err)
		require.NoError(s.Fatal(ctx, e.ID()))
	}
	// We should have 2 active entries, two successes, and two failures
	states, _, err = statesAndFailures(ctx, s, "myRange", values)
	require.NoError(err)
	expected = []irange.State{
		irange.Active,
		irange.Active,
		irange.Succeeded,
		irange.Succeeded,
		irange.Failed,
		irange.Failed,
	}
	require.Equal(expected, states)
	// Requeue the active states
	require.NoError(s.RequeueActive(ctx, "myRange"))
	states, _, err = statesAndFailures(ctx, s, "myRange", values)
	require.NoError(err)
	expected = []irange.State{
		irange.Pending,
		irange.Pending,
		irange.Succeeded,
		irange.Succeeded,
		irange.Failed,
		irange.Failed,
	}
	require.Equal(expected, states)
	// Requeue the succeeded states
	require.NoError(s.RequeueSucceeded(ctx, "myRange"))
	states, _, err = statesAndFailures(ctx, s, "myRange", values)
	require.NoError(err)
	expected = []irange.State{
		irange.Pending,
		irange.Pending,
		irange.Pending,
		irange.Pending,
		irange.Failed,
		irange.Failed,
	}
	require.Equal(expected, states)
	// Requeue the failed states
	require.NoError(s.RequeueFailed(ctx, "myRange"))
	states, _, err = statesAndFailures(ctx, s, "myRange", values)
	require.NoError(err)
	expected = []irange.State{
		irange.Pending,
		irange.Pending,
		irange.Pending,
		irange.Pending,
		irange.Pending,
		irange.Pending,
	}
	require.Equal(expected, states)
	// Delete the range
	require.NoError(s.Delete(ctx, "myRange"))
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// Run runs the standard tests on the irange.Storage s.
func Run(ctx context.Context, s irange.Storage, t *testing.T) {
	// Test that invalid names are handled properly
	t.Run("TestInvalidName", wrap(ctx, s, testInvalidName))
	// Test that Empty ranges behave correctly
	t.Run("TestEmpty", wrap(ctx, s, testEmpty))
	// Test Success, Fatal, Requeue, and Error.
	t.Run("TestIteration", wrap(ctx, s, testIteration))
	// Test that IsEqualTo behaves correctly
	t.Run("TestEquality", wrap(ctx, s, testEquality))
	// Test incorrect IDs are handled correctly
	t.Run("TestBadIDs", wrap(ctx, s, testBadIDs))
	// Test that unknown ranges are handled correctly
	t.Run("TestUnknownRanges", wrap(ctx, s, testUnknownRanges))
	// Test Create, List, and Delete
	t.Run("TestCreateListDelete", wrap(ctx, s, testCreateListDelete))
	// Test the timeout logic
	t.Run("TestTimeout", wrap(ctx, s, testTimeout))
	// Test cancellation
	t.Run("TestCancellation", wrap(ctx, s, testCancellation))
	// Test requeueing
	t.Run("TestRequeue", wrap(ctx, s, testRequeue))
}
