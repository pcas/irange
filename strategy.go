// Strategy defines strategies for reading entries from a range.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package irange

import (
	"bitbucket.org/pcas/irange/errors"
	"fmt"
)

// Strategy represents a strategy for picking the next element of a Range
type Strategy uint8

// Strategies for picking the next element of a Range
const (
	First   Strategy = iota // Pick the first element of the range
	Last                    // Pick the last element of the range
	Compact                 // Pick an element from the shortest interval
	Uniform                 // Pick a random element, with a uniform distribution
)

/////////////////////////////////////////////////////////////////////////
// Strategy functions
/////////////////////////////////////////////////////////////////////////

// String returns a string description of the strategy.
func (s Strategy) String() string {
	switch s {
	case First:
		return "first"
	case Last:
		return "last"
	case Compact:
		return "compact"
	case Uniform:
		return "uniform"
	default:
		return fmt.Sprintf("[unknown strategy (%d)]", s)
	}
}

// Iterator returns an iterator for the range R. Iteration proceeds using the strategy s.
func (s Strategy) Iterator(R Range) Iterator {
	return &strategyIterator{
		r: R,
		s: s,
	}
}

// Next returns the next element of the range R, along with the remaining range.
func (s Strategy) Next(R Range) (int64, Range, error) {
	// Handle the empty case
	if R.IsEmpty() {
		return 0, R, errors.ErrEmpty
	}
	// Switch according to the strategy
	var n int64
	switch s {
	case First:
		n = Span(R).Min()
	case Last:
		n = Span(R).Max()
	case Compact:
		// Find the shortest interval
		S := R.Intervals()
		minLength, minIdx := Len(S[0]), 0
		for i, I := range S[1:] {
			if size := Len(I); size < minLength {
				minLength, minIdx = size, i+1
			}
		}
		n = S[minIdx].Min()
	case Uniform:
		n = Random(R)
	default:
		return 0, R, fmt.Errorf("unknown strategy (%d)", s)
	}
	// Exclude the element from the range and return
	T, err := Exclude(R, n)
	return n, T, err
}
