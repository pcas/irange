// Json_test tests marshalling to JSON

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package irange

import (
	"encoding/json"
	"github.com/stretchr/testify/require"
	"testing"
)

/////////////////////////////////////////////////////////////////////////
// Test functions
/////////////////////////////////////////////////////////////////////////

// TestMarshalJSON tests MarshalJSON
func TestMarshalJSON(t *testing.T) {
	require := require.New(t)
	// test Empty ranges
	b, err := json.Marshal(Empty)
	require.NoError(err)
	require.Equal(b, []byte("[]"))
	R, err := Parse("[-5..-8,3..2,1..-1]")
	require.NoError(err)
	b, err = json.Marshal(R)
	require.NoError(err)
	require.Equal(b, []byte("[]"))
	// test non-Empty ranges
	tests := [][]string{
		{"[1..6,-7..-1]", "[[-7,-1],[1,6]]"},
		{"[3,5]", "[3,5]"},
		{"[3,5,6]", "[3,[5,6]]"},
	}
	for _, x := range tests {
		R, err := Parse(x[0])
		require.NoError(err)
		b, err = json.Marshal(R)
		require.NoError(err)
		require.Equal(b, []byte(x[1]))
	}
}
