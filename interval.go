// Interval defines an object representing an interval

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package irange

import (
	"bitbucket.org/pcas/irange/errors"
	"bitbucket.org/pcastools/bytesbuffer"
	"bitbucket.org/pcastools/stringsbuilder"
	"math/rand"
	"sort"
	"strconv"
	"strings"
	"time"
)

// Interval defines an interval [a,b] \cap \ZZ of integers.
type Interval interface {
	Range
	// Min returns the minimum value included in this interval. If the
	// interval is empty this always returns 0.
	Min() int64
	// Max returns the maximum value included in this interval. If the
	// interval is empty this always returns 0.
	Max() int64
}

// emptyInterval represents the empty interval.
type emptyInterval int

// Empty is an empty interval.
const Empty = emptyInterval(0)

// element represents the singleton interval a.
type element int64

// interval represents the interval [a,b] \cap \ZZ.
type interval struct {
	a int64
	b int64
}

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// init seeds the random number generator
func init() {
	rand.Seed(time.Now().UnixNano())
}

// parseInterval parses a string s representing an interval. The string s can be: the empty string "", in which case the empty interval is returned; a single integer "n", in which case the returned interval is the singleton n; or the string "a..b", in which case the returned interval agrees with that returned by ToInterval(a,b). Returns errors.ErrParse on failure.
func parseInterval(s string) (Interval, error) {
	// Trim any space and handle the empty string
	s = strings.TrimSpace(s)
	if len(s) == 0 {
		return Empty, nil
	}
	// Look for ".."
	pieces := strings.Split(s, "..")
	switch len(pieces) {
	case 1:
		// A single integer n
		n, err := strconv.ParseInt(pieces[0], 10, 64)
		if err != nil {
			return Empty, errors.ErrParse
		}
		return element(n), nil
	case 2:
		// An interval specifier a..b
		a, err := strconv.ParseInt(strings.TrimSpace(pieces[0]), 10, 64)
		if err != nil {
			return Empty, errors.ErrParse
		}
		b, err := strconv.ParseInt(strings.TrimSpace(pieces[1]), 10, 64)
		if err != nil {
			return Empty, errors.ErrParse
		}
		return ToInterval(a, b), nil
	default:
		// ".." should not occur more than once
		return Empty, errors.ErrParse
	}
}

// overlaps returns true if and only if the intersection of the intervals I and J is non-empty. If true, it also returns the intersection I \cap J and the union I \cup J.
func overlaps(I Interval, J Interval) (ok bool, meet Interval, join Interval) {
	// Handle the empty cases
	if I.IsEmpty() || J.IsEmpty() {
		return false, Empty, Empty
	}
	// Write I = [a,b] and J = [c,d]
	a, b, c, d := I.Min(), I.Max(), J.Min(), J.Max()
	if b < c {
		// a <= b < c <= d
		return false, Empty, Empty
	} else if b <= d && a < c {
		// a < c <= b <= d
		return true, ToInterval(c, b), ToInterval(a, d)
	} else if b <= d {
		// c <= a <= b <= d
		return true, I, J
	} else if a < c {
		// a < c <= d < b
		return true, J, I
	} else if a <= d {
		// c <= a <= d < b
		return true, ToInterval(a, d), ToInterval(c, b)
	}
	// c <= d < a <= b
	return false, Empty, Empty
}

// lessThan returns true if the interval I is considered to be less than the interval J. We sort by left-hand endpoints, breaking ties by sorting by right-hand endpoints. The empty interval is less than every non-empty interval.
func lessThan(I Interval, J Interval) bool {
	if I.IsEmpty() {
		return !J.IsEmpty()
	} else if J.IsEmpty() {
		return false
	}
	a, c := I.Min(), J.Min()
	return (a < c) || (a == c && I.Max() < J.Max())
}

// isSortedAndMerged checks whether the given intervals are already sorted and merged as determined by sortAndMerge.
func isSortedAndMerged(S []Interval) bool {
	I := Interval(Empty)
	for i, J := range S {
		if J.IsEmpty() {
			return false
		} else if i != 0 && I.Max()+1 >= J.Min() {
			return false
		}
		I = J
	}
	return true
}

// sortAndMerge sorts the given intervals in increasing order as determined by lessThan. Any empty intervals will be removed, and any intervals that can be combined into a single interval will be combined.
func sortAndMerge(S []Interval) []Interval {
	// Can we avoid any allocation?
	if isSortedAndMerged(S) {
		return S
	}
	// Copy the non-empty intervals
	T := make([]Interval, 0, len(S))
	for _, I := range S {
		if !I.IsEmpty() {
			T = append(T, I)
		}
	}
	// Handle the easy cases
	if len(T) == 0 {
		return nil
	} else if len(T) == 1 {
		return []Interval{T[0]}
	}
	// Sort the intervals
	sort.Slice(T, func(i, j int) bool { return lessThan(T[i], T[j]) })
	// Prepare the result
	U := make([]Interval, 1, len(T))
	// Move along the list of intervals, amalgamating as we go
	U[0] = T[0]
	i := 0
	for _, I := range T[1:] {
		// Do the intervals overlap?
		if ok, _, join := overlaps(U[i], I); ok {
			// They overlap, so expand the current interval
			U[i] = join
		} else if U[i].Max()+1 == I.Min() {
			// The intervals are adjacent, so expand the current interval
			U[i] = ToInterval(U[i].Min(), I.Max())
		} else {
			// Add this interval to the new list
			U = append(U, I)
			i++
		}
	}
	// If necessary, rebuild the slice before returning
	if len(U) < len(T) {
		Ucpy := make([]Interval, len(U))
		copy(Ucpy, U)
		U = Ucpy
	}
	return U
}

// findInterval returns true, j if n lies in S[j] and false if no such j exists. Here S is a slice of non-empty disjoint intervals in increasing order. That is, S[j].Max() < S[j+1].Min() for all j.
func findInterval(S []Interval, n int64) (bool, int) {
	j := sort.Search(len(S), func(i int) bool { return n <= S[i].Max() })
	if j < len(S) && Contains(S[j], n) {
		return true, j
	}
	return false, 0
}

// meetInterval returns the slice of intervals [I \cap J : J in S] with empty intervals removed.
func meetInterval(I Interval, S []Interval) []Interval {
	T := make([]Interval, 0, len(S))
	for _, J := range S {
		if ok, meet, _ := overlaps(I, J); ok {
			T = append(T, meet)
		}
	}
	return T
}

// complement returns I \setminus \cup S as a slice of intervals.
func complement(I Interval, S []Interval) []Interval {
	// Handle the case when I is empty
	if I.IsEmpty() {
		return nil
	}
	// Sort and amalgamate the intervals in S
	S = sortAndMerge(S)
	if len(S) == 0 {
		return []Interval{I}
	}
	// Compute the complement
	T := make([]Interval, 0, len(S)+1)
	a, b := I.Min(), I.Max()
	for _, J := range S {
		aa, bb := J.Min(), J.Max()
		if b < aa {
			if a <= b {
				T = append(T, ToInterval(a, b))
			}
			return T
		} else if a < aa {
			T = append(T, ToInterval(a, aa-1))
		}
		if bb >= a {
			a = bb + 1
		}
	}
	if a <= b {
		T = append(T, ToInterval(a, b))
	}
	return T
}

/////////////////////////////////////////////////////////////////////////
// emptyInterval functions
/////////////////////////////////////////////////////////////////////////

// IsEmpty returns true if and only if this interval is empty.
func (emptyInterval) IsEmpty() bool {
	return true
}

// Min returns the minimum value included in this interval. If the interval is empty this always returns 0.
func (emptyInterval) Min() int64 {
	return 0
}

// Max returns the maximum value included in this interval. If the interval is empty this always returns 0.
func (emptyInterval) Max() int64 {
	return 0
}

// Intervals returns a slice of intervals representing this interval.
func (emptyInterval) Intervals() []Interval {
	return nil
}

// String returns a string representation of the interval.
func (emptyInterval) String() string {
	return "[]"
}

// MarshalJSON returns a JSON description of the interval.
func (emptyInterval) MarshalJSON() ([]byte, error) {
	return []byte{'[', ']'}, nil
}

/////////////////////////////////////////////////////////////////////////
// element functions
/////////////////////////////////////////////////////////////////////////

// IsEmpty returns true if and only if this interval is empty.
func (n element) IsEmpty() bool {
	return false
}

// Min returns the minimum value included in this interval. If the interval is empty this always returns 0.
func (n element) Min() int64 {
	return int64(n)
}

// Max returns the maximum value included in this interval. If the interval is empty this always returns 0.
func (n element) Max() int64 {
	return int64(n)
}

// Intervals returns a slice of intervals representing this interval.
func (n element) Intervals() []Interval {
	return []Interval{n}
}

// String returns a string representation of the interval.
func (n element) String() string {
	return "[" + strconv.FormatInt(int64(n), 10) + "]"
}

// MarshalJSON returns a JSON description of the interval.
func (n element) MarshalJSON() ([]byte, error) {
	return []byte(n.String()), nil
}

/////////////////////////////////////////////////////////////////////////
// interval functions
/////////////////////////////////////////////////////////////////////////

// IsEmpty returns true if and only if this interval is empty.
func (I *interval) IsEmpty() bool {
	return false
}

// Min returns the minimum value included in this interval. If the interval is empty this always returns 0.
func (I *interval) Min() int64 {
	return I.a
}

// Max returns the maximum value included in this interval. If the interval is empty this always returns 0.
func (I *interval) Max() int64 {
	return I.b
}

// Intervals returns a slice of intervals representing this interval.
func (I *interval) Intervals() []Interval {
	return []Interval{I}
}

// String returns a string representation of the interval.
func (I *interval) String() string {
	b := stringsbuilder.New()
	defer stringsbuilder.Reuse(b)
	b.WriteByte('[')
	b.WriteString(strconv.FormatInt(I.a, 10))
	if I.a != I.b {
		b.WriteByte('.')
		b.WriteByte('.')
		b.WriteString(strconv.FormatInt(I.b, 10))
	}
	b.WriteByte(']')
	return b.String()
}

// MarshalJSON returns a JSON description of the interval.
func (I *interval) MarshalJSON() ([]byte, error) {
	// Fetch a bytes builder
	b := bytesbuffer.New()
	defer bytesbuffer.Reuse(b)
	// Write out the opening bracket
	b.WriteByte('[')
	// Write out the value
	if I.a == I.b {
		b.WriteString(strconv.FormatInt(I.a, 10))
	} else {
		b.WriteByte('[')
		b.WriteString(strconv.FormatInt(I.a, 10))
		b.WriteByte(',')
		b.WriteString(strconv.FormatInt(I.b, 10))
		b.WriteByte(']')
	}
	// Write out the closing bracket
	b.WriteByte(']')
	// Return a copy of the underlying bytes
	raw := b.Bytes()
	cpy := make([]byte, len(raw))
	copy(cpy, raw)
	return cpy, nil
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// ToInterval returns the interval [a,b]. If b < a then this interval will be empty.
func ToInterval(a int64, b int64) Interval {
	if b < a {
		return Empty
	} else if a == b {
		return element(a)
	}
	return &interval{
		a: a,
		b: b,
	}
}
