// Storage defines an interface satisfied by storage engines for Range objects

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package irange

import (
	"bitbucket.org/pcastools/ulid"
	"context"
	"fmt"
	"time"
)

// Entry holds metadata about an active entry in a range stored in a Range storage engine.
type Entry interface {
	ID() ID              // an ID that identifies the entry
	Value() int64        // the value of the entry
	Deadline() time.Time // the time at which the entry will go stale
	Failures() int       // the number of times this entry has failed
}

// ID uniquely identifies an active entry in a range.
type ID ulid.ULID

// NilID is a zero value for ID. It represents an invalid entry.
var NilID ID = ID(ulid.Nil)

// State records the state of an entry in a range.
type State uint8

// States of entries
const (
	Uninitialised State = iota
	Pending
	Active
	Succeeded
	Failed
)

// Metadata holds user-provided metadata describing a client
type Metadata struct {
	AppName  string // a name that identifies the client
	Hostname string // the hostname of the client
}

// Info describes an entry in a range stored in a Range storage system.
type Info interface {
	// Value returns the value of this entry.
	Value() int64
	// State returns the state of this entry.
	State() State
	// AppName returns the application name provided by the client to which
	// the entry was assigned, or the empty string if there is no such
	// client.
	AppName() string
	// Hostname returns the hostname provided by the client to which the
	// entry was assigned, or the empty string if there is no such client.
	Hostname() string
	// Start returns the time the entry was assigned to a client, or the
	// zero time if there is no such client.
	Start() time.Time
	// Deadline returns the time at which this entry will go stale, or the
	// zero time if the entry has not been assigned to a client.
	Deadline() time.Time
	// Failures returns the number of times this entry has failed.
	Failures() int
}

// Status describes the status of a range
type Status interface {
	Pending() Range   // The pending entries
	Active() Range    // The active entries
	Succeeded() Range // The entries that succeeded
	Failed() Range    // The entries that failed
}

// Storage is an interface satisfied by storage engines for Range objects. Ranges are stored by name, which must be nonempty and must not start or end with whitespace. Calls to Create, Delete, Next, Status, and Info with invalid names return errors.ErrInvalidName.
type Storage interface {
	// Create creates a range r with given name. Entries have the given
	// lifetime; they are retried at most maxRetries times on failure, and
	// at most maxConcurrency entries can be active at the same time. If a
	// range with that name already exists, Create returns
	// errors.ErrRangeExists.
	Create(ctx context.Context, name string, r Range, lifetime time.Duration, maxRetries int, maxConcurrency int) error
	// Delete deletes the range with the given name.
	Delete(ctx context.Context, name string) error
	// RequeueActive requeues all active entries in the range with the
	// given name.
	RequeueActive(ctx context.Context, name string) error
	// RequeueSucceeded requeues all entries that have succeeded in the
	// range with the given name.
	RequeueSucceeded(ctx context.Context, name string) error
	// RequeueFailed requeues all failed entries in the range with the
	// given name.
	RequeueFailed(ctx context.Context, name string) error
	// Next returns the next entry in the range with the given name, or
	// errors.ErrEmpty if no such entry exists. The caller identifies itself
	// to the storage engine via m.
	Next(ctx context.Context, name string, m *Metadata) (Entry, error)
	// Success indicates that the entry with the given ID has succeeded.
	Success(context.Context, ID) error
	// Error indicates that the entry with the given ID has failed and
	// should be retried.
	Error(context.Context, ID) error
	// Requeue indicates that the entry with the given ID should be
	// requeued, without incrementing the number of failures.
	Requeue(context.Context, ID) error
	// Fatal indicates that the entry with the given ID has failed and
	// should not be requeued.
	Fatal(context.Context, ID) error
	// List returns the names of all known ranges.
	List(ctx context.Context) ([]string, error)
	// Status returns the status of the range with the given name.
	Status(ctx context.Context, name string) (Status, error)
	// Info returns information about the given entry in the range with
	// the given name.
	Info(ctx context.Context, name string, entry int64) (Info, error)
}

/////////////////////////////////////////////////////////////////////////
// State functions
/////////////////////////////////////////////////////////////////////////

func (s State) String() string {
	switch s {
	case Uninitialised:
		return "uninitialised"
	case Pending:
		return "pending"
	case Active:
		return "active"
	case Succeeded:
		return "succeeded"
	case Failed:
		return "failed"
	}
	return fmt.Sprintf("[unknown state (%d)]", s)
}

/////////////////////////////////////////////////////////////////////////
// ID functions
/////////////////////////////////////////////////////////////////////////

// String returns a string representation of the ID.
func (id ID) String() string {
	return ulid.ULID(id).String()
}
