// Iterator constructs iterators from Range objects

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package irange

import (
	"bitbucket.org/pcas/irange/errors"
	"io"
)

// Iterator is the interface satisfied by iterators built from Range objects.
type Iterator interface {
	io.Closer
	// Err returns the last error, if any, encountered during iteration.
	// Err may be called after Close.
	Err() error
	// Next advances the iterator. Returns true on successful advance of
	// the iterator; false otherwise. Next must be called before the first
	// call to Value.
	Next() bool
	// Value returns the current value. This will panic if Next has not
	// been called or the iterator is closed.
	Value() int64
}

// strategyIterator is a range iterator that uses a strategy for iteration.
type strategyIterator struct {
	r          Range    // the underlying range
	s          Strategy // the iteration strategy
	n          int64    // the current value
	isClosed   bool     // are we closed?
	lastErr    error    // the last error encountered during iteration
	nextCalled bool     // has Next been called?
}

/////////////////////////////////////////////////////////////////////////
// strategyIterator functions
/////////////////////////////////////////////////////////////////////////

// Close closes the iterator, preventing further iteration. It returns nil.
func (itr *strategyIterator) Close() error {
	itr.isClosed = true
	return nil
}

// Err returns the last error, if any, encountered during iteration. Err may be called after Close.
func (itr *strategyIterator) Err() error {
	return itr.lastErr
}

// Next advances the iterator. Returns true on successful advance of the iterator; false otherwise. Next must be called before the first call to Value.
func (itr *strategyIterator) Next() bool {
	// Are we closed?
	if itr.isClosed {
		itr.lastErr = errors.ErrClosed
		return false
	}
	// Try to get the next element from the range
	n, r, err := itr.s.Next(itr.r)
	if err != nil {
		if err != errors.ErrEmpty {
			itr.lastErr = err
		}
		return false
	}
	// Update the value and range
	itr.n, itr.r = n, r
	// Record the fact that next was called
	itr.nextCalled = true
	return true
}

// Value returns the current value. This will panic if Next has not been called or the iterator is closed.
func (itr *strategyIterator) Value() int64 {
	if itr.isClosed {
		panic(errors.ErrClosed)
	} else if !itr.nextCalled {
		panic(errors.ErrUninitialised)
	}
	return itr.n
}
