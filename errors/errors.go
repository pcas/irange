// Errors defines an error type used to represent common irange errors.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package errors

import (
	"strconv"
)

// Code represents a common irange error.
type Code uint8

// The valid error codes
const (
	ErrEmpty = Code(iota)
	ErrParse
	ErrNotInRange
	ErrUninitialised
	ErrClosed
	ErrUnknownRange
	ErrRangeExists
	ErrUnknownID
	ErrInvalidName
	ErrNilConfiguration
	ErrNegativeTimeout
)

// codeToString maps error codes to strings.
var codeToString = map[Code]string{
	ErrEmpty:            "empty range",
	ErrParse:            "can't parse string to range",
	ErrNotInRange:       "integer not in range",
	ErrUninitialised:    "Next must be called before Value",
	ErrClosed:           "iterator is closed",
	ErrUnknownRange:     "unknown range",
	ErrRangeExists:      "range exists",
	ErrUnknownID:        "unknown ID",
	ErrInvalidName:      "illegal range name",
	ErrNilConfiguration: "illegal nil configuration",
	ErrNegativeTimeout:  "illegal negative timeout",
}

// Error is the interface satisfied by an error with a Code.
type Error interface {
	// Code returns the code associated with this error.
	Code() Code
	// Error returns a string description of the error.
	Error() string
}

/////////////////////////////////////////////////////////////////////////
// Code functions
/////////////////////////////////////////////////////////////////////////

// String returns a description of the error with error code c.
func (c Code) String() string {
	s, ok := codeToString[c]
	if !ok {
		s = "Unknown error (error code: " + strconv.Itoa(int(c)) + ")"
	}
	return s
}

// Error returns a description of the error with error code c.
func (c Code) Error() string {
	return c.String()
}

// Code returns the error code c.
func (c Code) Code() Code {
	return c
}
