// Client describes the client view of the rangedb server.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package rangedb

import (
	"bitbucket.org/pcas/irange"
	irerrors "bitbucket.org/pcas/irange/errors"
	"bitbucket.org/pcas/irange/rangedb/internal/rangerpc"
	"bitbucket.org/pcastools/grpcutil"
	"bitbucket.org/pcastools/grpcutil/grpcdialer"
	"bitbucket.org/pcastools/grpcutil/grpcs2"
	"bitbucket.org/pcastools/log"
	"context"
	"errors"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
	"google.golang.org/grpc/credentials/insecure"
	"google.golang.org/grpc/metadata"
	"google.golang.org/protobuf/types/known/emptypb"
	"io"
	"net/url"
	"strconv"
	"strings"
	"time"
)

// Client is the client-view of the server.
type Client struct {
	log.BasicLogable
	io.Closer
	client rangerpc.RangeClient // The gRPC client
}

// errors
var (
	ErrNilClient = errors.New("uninitialised client")
)

// Assert that the client satisfies the irange.Storage interface
var _ irange.Storage = &Client{}

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// validateName validates the range name s. It returns ErrInvalidName if s is trivial, or begins or ends with whitespace.
func validateName(s string) error {
	if t := strings.TrimSpace(s); s == "" || t != s {
		return irerrors.ErrInvalidName
	}
	return nil
}

// createDialFunc returns the dial function for the client. Assumes that the URI has scheme "tcp" or "ws", and that a port number is set.
func createDialFunc(uri string, lg log.Interface) grpc.DialOption {
	// Parse the URI
	u, err := url.ParseRequestURI(uri)
	if err != nil {
		panic("invalid URI: " + uri)
	}
	switch u.Scheme {
	case "tcp":
		// Connect via a TCP socket
		port, err := strconv.Atoi(u.Port())
		if err != nil {
			panic("invalid URI: " + uri)
		}
		return grpcdialer.TCPDialer(u.Hostname(), port, lg)
	case "ws":
		// Connect via a websocket
		return grpcdialer.WebSocketDialer(uri, lg)
	default:
		// Unknown scheme
		panic("unsupported URI scheme: " + u.Scheme)
	}
}

// setClientMetadata sets the client metadata.
func setClientMetadata(m *irange.Metadata) metadata.MD {
	md := make(map[string]string, 2)
	if len(m.AppName) != 0 {
		md[mdAppName] = m.AppName
	}
	if len(m.Hostname) != 0 {
		md[mdHostname] = m.Hostname
	}
	return metadata.New(md)
}

/////////////////////////////////////////////////////////////////////////
// Client functions
/////////////////////////////////////////////////////////////////////////

// NewClient returns a new client view of the server specified by the configuration cfg.
func NewClient(ctx context.Context, cfg *ClientConfig) (*Client, error) {
	// Sanity checks
	if err := cfg.Validate(); err != nil {
		return nil, err
	}
	// Create a new client
	c := &Client{}
	// Set the dial options
	dialOptions := []grpc.DialOption{
		createDialFunc(cfg.URI(), c.Log()),
		grpc.WithDefaultCallOptions(
			grpc.UseCompressor(grpcs2.Name),
		),
		grpc.WithChainUnaryInterceptor(
			unaryClientErrorInterceptor,
		),
	}
	// Add the transport credentials
	var creds credentials.TransportCredentials
	if cfg.SSLDisabled {
		creds = insecure.NewCredentials()
	} else {
		creds = grpcutil.NewClientTLS(cfg.SSLCert)
	}
	dialOptions = append(dialOptions, grpc.WithTransportCredentials(creds))
	// Build a connection to the gRPC server
	conn, err := grpc.DialContext(ctx, cfg.Address.Hostname(), dialOptions...)
	if err != nil {
		return nil, err
	}
	// Save the connection on the client and return
	c.Closer = conn
	c.client = rangerpc.NewRangeClient(conn)
	return c, nil
}

// Create creates a range r with given name. Entries have the given lifetime; they are retried at most maxRetries times on failure, and at most maxConcurrency entries can be active at the same time. If a range with that name already exists, Create returns ErrRangeExists.
func (c *Client) Create(ctx context.Context, name string, r irange.Range, lifetime time.Duration, maxRetries int, maxConcurrency int) error {
	// Sanity checks
	if c == nil || c.client == nil {
		return ErrNilClient
	} else if err := validateName(name); err != nil {
		return err
	}
	// Hand off
	_, err := c.client.Create(ctx, rangerpc.FromRangeSpecification(name, r, lifetime, maxRetries, maxConcurrency))
	return err
}

// Delete deletes the range with the given name
func (c *Client) Delete(ctx context.Context, name string) error {
	// Sanity checks
	if c == nil || c.client == nil {
		return ErrNilClient
	} else if err := validateName(name); err != nil {
		return err
	}
	// Hand off
	_, err := c.client.Delete(ctx, rangerpc.FromName(name))
	return err
}

// RequeueActive requeues all active entries in the range with the given name
func (c *Client) RequeueActive(ctx context.Context, name string) error {
	// Sanity checks
	if c == nil || c.client == nil {
		return ErrNilClient
	} else if err := validateName(name); err != nil {
		return err
	}
	// Hand off
	_, err := c.client.RequeueActive(ctx, rangerpc.FromName(name))
	return err
}

// RequeueSucceeded requeues all entries that have succeeded in the range with the given name
func (c *Client) RequeueSucceeded(ctx context.Context, name string) error {
	// Sanity checks
	if c == nil || c.client == nil {
		return ErrNilClient
	} else if err := validateName(name); err != nil {
		return err
	}
	// Hand off
	_, err := c.client.RequeueSucceeded(ctx, rangerpc.FromName(name))
	return err
}

// RequeueFailed requeues all failed entries in the range with the given name
func (c *Client) RequeueFailed(ctx context.Context, name string) error {
	// Sanity checks
	if c == nil || c.client == nil {
		return ErrNilClient
	} else if err := validateName(name); err != nil {
		return err
	}
	// Hand off
	_, err := c.client.RequeueFailed(ctx, rangerpc.FromName(name))
	return err
}

// Next returns the next entry in the range with the given name, or ErrEmpty if no such entry exists. The caller identifies itself to the storage engine via m.
func (c *Client) Next(ctx context.Context, name string, m *irange.Metadata) (irange.Entry, error) {
	// Sanity checks
	if c == nil || c.client == nil {
		return nil, ErrNilClient
	} else if err := validateName(name); err != nil {
		return nil, err
	}
	// Add the client application name to the metadata on the context
	newCtx := metadata.NewOutgoingContext(ctx, setClientMetadata(m))
	// Hand off
	e, err := c.client.Next(newCtx, rangerpc.FromName(name))
	if err != nil {
		return nil, err
	}
	// Convert the return value
	return rangerpc.ToEntry(e)
}

// Success indicates that the entry with the given ID has succeeded.
func (c *Client) Success(ctx context.Context, id irange.ID) error {
	// Sanity checks
	if c == nil || c.client == nil {
		return ErrNilClient
	}
	// Hand off
	_, err := c.client.Success(ctx, rangerpc.FromID(id))
	return err
}

// Error indicates that the entry with the given ID has failed and should be retried.
func (c *Client) Error(ctx context.Context, id irange.ID) error {
	// Sanity checks
	if c == nil || c.client == nil {
		return ErrNilClient
	}
	// Hand off
	_, err := c.client.Error(ctx, rangerpc.FromID(id))
	return err
}

// Requeue indicates that the entry with the given ID should be requeued, without incrementing the number of failures.
func (c *Client) Requeue(ctx context.Context, id irange.ID) error {
	// Sanity checks
	if c == nil || c.client == nil {
		return ErrNilClient
	}
	// Hand off
	_, err := c.client.Requeue(ctx, rangerpc.FromID(id))
	return err
}

// Fatal indicates that the entry with the given ID has failed and should not be requeued.
func (c *Client) Fatal(ctx context.Context, id irange.ID) error {
	// Sanity checks
	if c == nil || c.client == nil {
		return ErrNilClient
	}
	// Hand off
	_, err := c.client.Fatal(ctx, rangerpc.FromID(id))
	return err
}

// List returns the names of all known ranges.
func (c *Client) List(ctx context.Context) ([]string, error) {
	// Sanity checks
	if c == nil || c.client == nil {
		return nil, ErrNilClient
	}
	// Hand off
	lr, err := c.client.List(ctx, &emptypb.Empty{})
	if err != nil {
		return nil, err
	}
	// Convert the return value
	return rangerpc.ToNames(lr)
}

// Status returns the status of the range with the given name.
func (c *Client) Status(ctx context.Context, name string) (irange.Status, error) {
	// Sanity checks
	if c == nil || c.client == nil {
		return nil, ErrNilClient
	} else if err := validateName(name); err != nil {
		return nil, err
	}
	// Hand off
	sr, err := c.client.Status(ctx, rangerpc.FromName(name))
	if err != nil {
		return nil, err
	}
	// Convert the return value
	return rangerpc.ToStatus(sr)
}

// Info returns information about the given entry in the range with the given name.
func (c *Client) Info(ctx context.Context, name string, entry int64) (irange.Info, error) {
	// Sanity checks
	if c == nil || c.client == nil {
		return nil, ErrNilClient
	} else if err := validateName(name); err != nil {
		return nil, err
	}
	// Hand off
	ir, err := c.client.Info(ctx, rangerpc.FromNameAndEntry(name, entry))
	if err != nil {
		return nil, err
	}
	// Convert the return value
	return rangerpc.ToInfo(ir)
}
