// Serveroptions provides configuration options for a rangedb server.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package rangedb

import (
	"bitbucket.org/pcas/irange"
	"errors"
)

// Option sets options on an irange server.
type Option interface {
	apply(*serverOptions) error
}

// funcOption wraps a function that modifies Options into an implementation of the Option interface.
type funcOption struct {
	f func(*serverOptions) error
}

// apply calls the wrapped function f on the given Options.
func (h *funcOption) apply(do *serverOptions) error {
	return h.f(do)
}

// newFuncOption returns a funcOption wrapping f.
func newFuncOption(f func(*serverOptions) error) *funcOption {
	return &funcOption{
		f: f,
	}
}

// serverOptions are the options on an ranged server.
type serverOptions struct {
	Storage irange.Storage // The underlying storage engine
	SSLCert []byte         // The SSL (public) certificate
	SSLKey  []byte         // The SSL (private) key
}

/////////////////////////////////////////////////////////////////////////
// Options functions
/////////////////////////////////////////////////////////////////////////

// parseOptions parses the given optional functions.
func parseOptions(options ...Option) (*serverOptions, error) {
	// Create the default options
	opts := &serverOptions{}
	// Set the options
	for _, h := range options {
		if err := h.apply(opts); err != nil {
			return nil, err
		}
	}
	// Do we have storage allocated?
	if opts.Storage == nil {
		return nil, errors.New("missing storage engine")
	}
	return opts, nil
}

// SSLCertAndKey adds the given SSL public certificate and private key to the server.
func SSLCertAndKey(crt []byte, key []byte) Option {
	// Copy the certificate and key
	crtCopy := make([]byte, len(crt))
	copy(crtCopy, crt)
	keyCopy := make([]byte, len(key))
	copy(keyCopy, key)
	// Return the option
	return newFuncOption(func(opts *serverOptions) error {
		if len(crtCopy) == 0 {
			return errors.New("missing SSL certificate")
		} else if len(keyCopy) == 0 {
			return errors.New("missing SSL private key")
		}
		opts.SSLCert = crtCopy
		opts.SSLKey = keyCopy
		return nil
	})
}

// WithStorage sets the underlying storage engine for the server.
func WithStorage(engine irange.Storage) Option {
	return newFuncOption(func(opts *serverOptions) error {
		opts.Storage = engine
		return nil
	})
}
