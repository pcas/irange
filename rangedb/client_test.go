// Client_test provides tests for the rangedb package.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package rangedb

import (
	"bitbucket.org/pcas/irange/inmem"
	"bitbucket.org/pcas/irange/internal/irangetest"
	"bitbucket.org/pcastools/address"
	"context"
	"net"
	"strconv"
	"testing"
	"time"
)

const (
	testAddr = "localhost"
	testPort = 45557
)

func startServer(ctx context.Context) (<-chan struct{}, error) {
	// Establish a connection
	l, err := net.Listen("tcp", testAddr+":"+strconv.Itoa(testPort))
	if err != nil {
		return nil, err
	}
	// Create the server
	s, err := NewServer(
		WithStorage(inmem.New(nil)),
	)
	if err != nil {
		l.Close()
		return nil, err
	}
	// Run the server in a new goroutine
	doneC := make(chan struct{})
	go func() {
		// Defer cleanup
		defer func() {
			l.Close()
			close(doneC)
		}()
		// Stop the server when the context fires
		go func() {
			<-ctx.Done()
			s.GracefulStop()
		}()
		// Start serving
		s.Serve(l)
	}()
	return doneC, nil
}

func connectClient(ctx context.Context) (*Client, error) {
	// Create the address
	addr, err := address.NewTCP(testAddr, testPort)
	if err != nil {
		return nil, err
	}
	// Create the client config
	cfg := DefaultConfig()
	cfg.Address = addr
	cfg.SSLDisabled = true
	// Open the client connection
	return NewClient(ctx, cfg)
}

func TestAll(t *testing.T) {
	// Create a context with reasonable timeout for the tests to run
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	// Start the server
	doneC, err := startServer(ctx)
	if err != nil {
		cancel()
		t.Fatalf("unable to start server: %v", err)
	}
	defer func() {
		cancel()
		<-doneC
	}()
	// Open a client connection
	c, err := connectClient(ctx)
	if err != nil {
		t.Fatalf("unable to create new client: %v", err)
	}
	// Run the tests
	irangetest.Run(ctx, c, t)
	// Close the client
	if err = c.Close(); err != nil {
		t.Fatalf("error closing client: %v", err)
	}
}
