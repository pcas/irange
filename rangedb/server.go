// Server handles connections from a rangedb client.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package rangedb

import (
	"bitbucket.org/pcas/irange"
	"bitbucket.org/pcas/irange/rangedb/internal/rangerpc"
	"bitbucket.org/pcas/metrics"
	"bitbucket.org/pcas/metrics/grpcmetrics"
	"bitbucket.org/pcastools/grpcutil"
	"bitbucket.org/pcastools/grpcutil/grpclog"
	"bitbucket.org/pcastools/log"
	"context"
	"errors"
	"fmt"
	"github.com/grpc-ecosystem/go-grpc-middleware"
	"google.golang.org/grpc"
	"google.golang.org/grpc/health"
	"google.golang.org/grpc/health/grpc_health_v1"
	"google.golang.org/grpc/metadata"
	"google.golang.org/protobuf/types/known/emptypb"

	// Make the compressors available
	_ "bitbucket.org/pcastools/grpcutil/grpcs2"
	_ "bitbucket.org/pcastools/grpcutil/grpcsnappy"
	_ "google.golang.org/grpc/encoding/gzip"
)

// The default ports that the rangedb server listens on.
const (
	DefaultTCPPort = 12359
	DefaultWSPort  = 80
)

// The metadata keys.
const (
	mdAppName  = "app_name" // The client application's name (optional)
	mdHostname = "hostname" // The client hostname
)

// rangedbServer implements the tasks on the gRPC server.
type rangedbServer struct {
	rangerpc.UnimplementedRangeServer
	log.BasicLogable
	metrics.BasicMetricsable
	s irange.Storage // The underlying storage engine
}

// Server handles client communication.
type Server struct {
	log.SetLoggerer
	metrics.SetMetricser
	grpcutil.Server
}

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// getMetadata reads the user-supplied metadata from the client metadata on the incoming context.
func getMetadata(ctx context.Context) (*irange.Metadata, error) {
	// Recover the client's metadata from the context
	md, ok := metadata.FromIncomingContext(ctx)
	if !ok {
		return nil, errors.New("failed to recover metadata")
	}
	// Extract the application name
	l := md.Get(mdAppName)
	if len(l) == 0 {
		return nil, fmt.Errorf("missing %s field in metadata", mdAppName)
	}
	appName := l[0]
	// Extract the hostname
	l = md.Get(mdHostname)
	if len(l) == 0 {
		return nil, fmt.Errorf("missing %s field in metadata", mdHostname)
	}
	hostName := l[0]
	return &irange.Metadata{
		AppName:  appName,
		Hostname: hostName,
	}, nil
}

/////////////////////////////////////////////////////////////////////////
// rangedbServer functions
/////////////////////////////////////////////////////////////////////////

// Create creates a range.
func (s *rangedbServer) Create(ctx context.Context, rs *rangerpc.RangeSpecification) (*emptypb.Empty, error) {
	// Sanity check
	if rs == nil {
		return &emptypb.Empty{}, errors.New("nil range specification in Create")
	}
	// Hand off to the storage engine
	name, r, lifetime, maxRetries, maxConcurrency, err := rangerpc.ToRangeSpecification(rs)
	if err != nil {
		return &emptypb.Empty{}, err
	}
	return &emptypb.Empty{}, s.s.Create(ctx, name, r, lifetime, maxRetries, maxConcurrency)
}

// Delete deletes a range.
func (s *rangedbServer) Delete(ctx context.Context, nm *rangerpc.Name) (*emptypb.Empty, error) {
	// Sanity check
	if nm == nil {
		return &emptypb.Empty{}, errors.New("nil name in Delete")
	}
	// Hand off to the storage engine
	name, err := rangerpc.ToName(nm)
	if err != nil {
		return &emptypb.Empty{}, err
	}
	return &emptypb.Empty{}, s.s.Delete(ctx, name)
}

// RequeueActive requeues all active entries in a range.
func (s *rangedbServer) RequeueActive(ctx context.Context, nm *rangerpc.Name) (*emptypb.Empty, error) {
	// Sanity check
	if nm == nil {
		return &emptypb.Empty{}, errors.New("nil name in RequeueActive")
	}
	// Hand off to the storage engine
	name, err := rangerpc.ToName(nm)
	if err != nil {
		return &emptypb.Empty{}, err
	}
	return &emptypb.Empty{}, s.s.RequeueActive(ctx, name)
}

// RequeueSucceeded requeues all entries in a range that have succeeded.
func (s *rangedbServer) RequeueSucceeded(ctx context.Context, nm *rangerpc.Name) (*emptypb.Empty, error) {
	// Sanity check
	if nm == nil {
		return &emptypb.Empty{}, errors.New("nil name in RequeueSucceeded")
	}
	// Hand off to the storage engine
	name, err := rangerpc.ToName(nm)
	if err != nil {
		return &emptypb.Empty{}, err
	}
	return &emptypb.Empty{}, s.s.RequeueSucceeded(ctx, name)
}

// RequeueFailed requeues all failed entries in a range.
func (s *rangedbServer) RequeueFailed(ctx context.Context, nm *rangerpc.Name) (*emptypb.Empty, error) {
	// Sanity check
	if nm == nil {
		return &emptypb.Empty{}, errors.New("nil name in RequeueFailed")
	}
	// Hand off to the storage engine
	name, err := rangerpc.ToName(nm)
	if err != nil {
		return &emptypb.Empty{}, err
	}
	return &emptypb.Empty{}, s.s.RequeueFailed(ctx, name)
}

// Next returns the next entry in a range.
func (s *rangedbServer) Next(ctx context.Context, nm *rangerpc.Name) (*rangerpc.Entry, error) {
	// Sanity check
	if nm == nil {
		return &rangerpc.Entry{}, errors.New("nil Name in Next")
	}
	// Grab the user-provided metadata from the metadata on the context
	m, err := getMetadata(ctx)
	if err != nil {
		return &rangerpc.Entry{}, err
	}
	// Hand off to the storage engine
	var e irange.Entry
	if name, err := rangerpc.ToName(nm); err != nil {
		return &rangerpc.Entry{}, err
	} else if e, err = s.s.Next(ctx, name, m); err != nil {
		return &rangerpc.Entry{}, err
	}
	// Convert the return value
	return rangerpc.FromEntry(e)
}

// Success indicates that a given entry has succeeded.
func (s *rangedbServer) Success(ctx context.Context, id *rangerpc.ID) (*emptypb.Empty, error) {
	// Sanity check
	if id == nil {
		return &emptypb.Empty{}, errors.New("nil ID in Success")
	}
	// Hand off to the storage engine
	u, err := rangerpc.ToID(id)
	if err != nil {
		return &emptypb.Empty{}, err
	}
	return &emptypb.Empty{}, s.s.Success(ctx, u)
}

// Error indicates that a given entry has failed and should be retried.
func (s *rangedbServer) Error(ctx context.Context, id *rangerpc.ID) (*emptypb.Empty, error) {
	// Sanity check
	if id == nil {
		return &emptypb.Empty{}, errors.New("nil ID in Error")
	}
	// Hand off to the storage engine
	u, err := rangerpc.ToID(id)
	if err != nil {
		return &emptypb.Empty{}, err
	}
	return &emptypb.Empty{}, s.s.Error(ctx, u)
}

// Requeue indicates that a given entry should be retried, without incrementing the number of failures.
func (s *rangedbServer) Requeue(ctx context.Context, id *rangerpc.ID) (*emptypb.Empty, error) {
	// Sanity check
	if id == nil {
		return &emptypb.Empty{}, errors.New("nil ULID in Requeue")
	}
	// Hand off to the storage engine
	u, err := rangerpc.ToID(id)
	if err != nil {
		return &emptypb.Empty{}, err
	}
	return &emptypb.Empty{}, s.s.Requeue(ctx, u)
}

// Fatal indicates that a given entry has failed and should not be retried.
func (s *rangedbServer) Fatal(ctx context.Context, id *rangerpc.ID) (*emptypb.Empty, error) {
	// Sanity check
	if id == nil {
		return &emptypb.Empty{}, errors.New("nil ULID in Fatal")
	}
	// Hand off to the storage engine
	u, err := rangerpc.ToID(id)
	if err != nil {
		return &emptypb.Empty{}, err
	}
	return &emptypb.Empty{}, s.s.Fatal(ctx, u)
}

// List returns the names of all known ranges.
func (s *rangedbServer) List(ctx context.Context, _ *emptypb.Empty) (*rangerpc.ListResponse, error) {
	// Hand off to the storage engine
	S, err := s.s.List(ctx)
	if err != nil {
		return &rangerpc.ListResponse{}, err
	}
	return rangerpc.FromNames(S), nil
}

// Status returns the remaining, active, succeeded, and failed entries in a range.
func (s *rangedbServer) Status(ctx context.Context, nm *rangerpc.Name) (*rangerpc.StatusResponse, error) {
	// Sanity check
	if nm == nil {
		return &rangerpc.StatusResponse{}, errors.New("nil Name in Status")
	}
	// Hand off to the storage engine
	name, err := rangerpc.ToName(nm)
	if err != nil {
		return &rangerpc.StatusResponse{}, err
	}
	st, err := s.s.Status(ctx, name)
	if err != nil {
		return &rangerpc.StatusResponse{}, err
	}
	return rangerpc.FromStatus(st)
}

// Info returns information about an entry in a range.
func (s *rangedbServer) Info(ctx context.Context, ne *rangerpc.NameAndEntry) (*rangerpc.InfoResponse, error) {
	// Sanity check
	if ne == nil {
		return &rangerpc.InfoResponse{}, errors.New("nil NameAndEntry in Info")
	}
	// Hand off to the storage engine
	var result irange.Info
	if name, entry, err := rangerpc.ToNameAndEntry(ne); err != nil {
		return &rangerpc.InfoResponse{}, err
	} else if result, err = s.s.Info(ctx, name, entry); err != nil {
		return &rangerpc.InfoResponse{}, err
	}
	// Convert the return value
	return rangerpc.FromInfo(result)
}

/////////////////////////////////////////////////////////////////////////
// Server functions
/////////////////////////////////////////////////////////////////////////

// NewServer returns a new rangedb server.
func NewServer(options ...Option) (*Server, error) {
	// Parse the options
	opts, err := parseOptions(options...)
	if err != nil {
		return nil, err
	}
	// Create the new server instance
	m := &rangedbServer{s: opts.Storage}
	// Create the gRPC options
	serverOptions := []grpc.ServerOption{
		grpc.UnaryInterceptor(grpc_middleware.ChainUnaryServer(
			unaryServerErrorInterceptor,
			grpclog.UnaryServerInterceptor(m.Log()),
			grpcmetrics.UnaryServerInterceptor(m.Metrics()),
		)),
	}
	// Add the SSL credentials to the gRPC options
	if len(opts.SSLKey) != 0 {
		creds, err := grpcutil.NewServerTLS(opts.SSLCert, opts.SSLKey)
		if err != nil {
			return nil, err
		}
		serverOptions = append(serverOptions, grpc.Creds(creds))
	}
	// Create the gRPC server and register our implementation
	s := grpc.NewServer(serverOptions...)
	rangerpc.RegisterRangeServer(s, m)
	// Bind the health data
	grpc_health_v1.RegisterHealthServer(s, health.NewServer())
	// Wrap and return the server
	return &Server{
		SetLoggerer:  m,
		SetMetricser: m,
		Server:       s,
	}, nil
}
