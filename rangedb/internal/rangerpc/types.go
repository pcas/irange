// Types defines types that satisfy the interfaces in the irange Storage API.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package rangerpc

import (
	"bitbucket.org/pcas/irange"
	"time"
)

// REntry holds metadata about an active entry in a range.
type rEntry struct {
	id       irange.ID // a ULID for the entry
	value    int64     // the value of this entry
	deadline time.Time // the time at which this entry will go stale
	failures int       // the number of times this entry has failed
}

// Entry satisfies the irange.Entry interface
var _ irange.Entry = &rEntry{}

// RInfo describes an entry in a range.
type rInfo struct {
	value    int64        // the value of this entry
	state    irange.State // the state of this entry
	appname  string       // the application name provided by the client to which the entry was assigned
	hostname string       // the hostname provided by the client to which the entry was assigned
	start    time.Time    // the time the entry was assigned
	deadline time.Time    // the time at which this entry will go stale
	failures int          // the number of times this entry has failed
}

// Info satisfies the irange.Info interface
var _ irange.Info = &rInfo{}

// RStatus describes the status of a range.
type rStatus struct {
	pending   irange.Range // The pending entries
	active    irange.Range // The active entries
	succeeded irange.Range // The entries that succeeded
	failed    irange.Range // The entries that failed
}

// Status satisfies the irange.Status interface
var _ irange.Status = &rStatus{}

/////////////////////////////////////////////////////////////////////////
// Status functions
/////////////////////////////////////////////////////////////////////////

// Pending returns the pending entries.
func (st *rStatus) Pending() irange.Range {
	if st == nil {
		return irange.Empty
	}
	return st.pending
}

// Active returns the active entries.
func (st *rStatus) Active() irange.Range {
	if st == nil {
		return irange.Empty
	}
	return st.active
}

// Succeeded returns the entries that have succeeded.
func (st *rStatus) Succeeded() irange.Range {
	if st == nil {
		return irange.Empty
	}
	return st.succeeded
}

// Failed returns the entries that have failed.
func (st *rStatus) Failed() irange.Range {
	if st == nil {
		return irange.Empty
	}
	return st.failed
}

/////////////////////////////////////////////////////////////////////////
// Info functions
/////////////////////////////////////////////////////////////////////////

// Value returns the value of this entry.
func (inf *rInfo) Value() int64 {
	if inf == nil {
		return 0
	}
	return inf.value
}

// State returns the state of this entry.
func (inf *rInfo) State() irange.State {
	if inf == nil {
		return irange.Uninitialised
	}
	return inf.state
}

// AppName returns the application name provided by the client to which this entry was assigned, or the empty string if there is no such client.
func (inf *rInfo) AppName() string {
	if inf == nil || inf.state != irange.Active {
		return ""
	}
	return inf.appname
}

// Hostname returns the hostname provided by the client to which this entry was assigned, or the empty string if there is no such client.
func (inf *rInfo) Hostname() string {
	if inf == nil || inf.state != irange.Active {
		return ""
	}
	return inf.hostname
}

// Start returns the time at which this entry was assigned to a client, or the zero time if there is no such client.
func (inf *rInfo) Start() time.Time {
	if inf == nil || inf.state != irange.Active {
		return time.Time{}
	}
	return inf.start
}

// Deadline returns the time at which this entry will go stale, or the zero time if this entry has not been assigned to a client.
func (inf *rInfo) Deadline() time.Time {
	if inf == nil || inf.state != irange.Active {
		return time.Time{}
	}
	return inf.deadline
}

// Failures returns the number of times this entry has failed.
func (inf *rInfo) Failures() int {
	if inf == nil {
		return 0
	}
	return inf.failures
}

/////////////////////////////////////////////////////////////////////////
// Entry functions
/////////////////////////////////////////////////////////////////////////

// ID returns the ID of the entry.
func (e *rEntry) ID() irange.ID {
	if e == nil {
		return irange.NilID
	}
	return e.id
}

// Value returns the value of the entry.
func (e *rEntry) Value() int64 {
	if e == nil {
		return 0
	}
	return e.value
}

// Deadline returns the time at which this entry will go stale.
func (e *rEntry) Deadline() time.Time {
	if e == nil {
		return time.Time{}
	}
	return e.deadline
}

// Failures returns the number of times that this entry has failed.
func (e *rEntry) Failures() int {
	if e == nil {
		return 0
	}
	return e.failures
}
