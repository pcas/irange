//go:generate protoc -I=../proto/ --go_opt=paths=source_relative --go-grpc_opt=paths=source_relative --go_out=. --go-grpc_out=. range.proto

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package rangerpc

import (
	"bitbucket.org/pcas/irange"
	"bitbucket.org/pcastools/stringsbuilder"
	"bitbucket.org/pcastools/ulid"
	"errors"
	"google.golang.org/protobuf/types/known/durationpb"
	"google.golang.org/protobuf/types/known/timestamppb"
	"strconv"
	"time"
)

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// idFromString attempts to convert the string s to an irange.ID.
func idFromString(s string) (irange.ID, error) {
	u, err := ulid.FromString(s)
	if err != nil {
		return irange.NilID, err
	}
	return irange.ID(u), nil
}

// rangeToString converts the range to a string.
func rangeToString(R irange.Range) string {
	if R.IsEmpty() {
		return "[]"
	} else if I, ok := R.(irange.Interval); ok {
		min, max := I.Min(), I.Max()
		if min == max {
			return "[" + strconv.FormatInt(min, 10) + "]"
		}
		return "[" + strconv.FormatInt(min, 10) + ".." + strconv.FormatInt(max, 10) + "]"
	}
	b := stringsbuilder.New()
	defer stringsbuilder.Reuse(b)
	b.WriteByte('[')
	for i, I := range R.Intervals() {
		if i != 0 {
			b.WriteByte(',')
		}
		min, max := I.Min(), I.Max()
		b.WriteString(strconv.FormatInt(min, 10))
		if min != max {
			b.WriteByte('.')
			b.WriteByte('.')
			b.WriteString(strconv.FormatInt(max, 10))
		}
	}
	b.WriteByte(']')
	return b.String()
}

/////////////////////////////////////////////////////////////////////////
// Conversion functions
/////////////////////////////////////////////////////////////////////////

// FromName converts the string s to a *Name.
func FromName(s string) *Name {
	return &Name{Name: s}
}

// ToName converts the given *Name to a string.
func ToName(nm *Name) (string, error) {
	if nm == nil {
		return "", errors.New("nil *Name in ToName")
	}
	return nm.GetName(), nil
}

// FromEntry converts the given irange.Entry to a *Entry.
func FromEntry(e irange.Entry) (*Entry, error) {
	return &Entry{
		Id:       e.ID().String(),
		Value:    e.Value(),
		Deadline: timestamppb.New(e.Deadline()),
		Failures: int64(e.Failures()),
	}, nil
}

// ToEntry converts the given *Entry to an irange.Entry.
func ToEntry(e *Entry) (irange.Entry, error) {
	if e == nil {
		return nil, errors.New("nil *Entry in ToEntry")
	}
	// Convert the ID
	id, err := idFromString(e.GetId())
	if err != nil {
		return nil, err
	}
	// Package up the result and return
	return &rEntry{
		id:       id,
		value:    e.GetValue(),
		deadline: e.GetDeadline().AsTime(),
		failures: int(e.GetFailures()),
	}, nil
}

// FromRangeSpecification converts the the given name, range r, lifetime, maximum number of retries and maximum concurrency to a *RangeSpecification.
func FromRangeSpecification(name string, r irange.Range, lifetime time.Duration, maxRetries int, maxConcurrency int) *RangeSpecification {
	return &RangeSpecification{
		Name:           name,
		Range:          rangeToString(r),
		Lifetime:       durationpb.New(lifetime),
		MaxRetries:     int64(maxRetries),
		MaxConcurrency: int64(maxConcurrency),
	}
}

// ToRangeSpecification converts the given *RangeSpecification to a name, range r, lifetime, maximum number of retries and maximum concurrency.
func ToRangeSpecification(rs *RangeSpecification) (name string, r irange.Range, lifetime time.Duration, maxRetries int, maxConcurrency int, err error) {
	if rs == nil {
		err = errors.New("nil *RangeSpecification in ToRangeSpecification")
		return
	}
	name = rs.GetName()
	if r, err = irange.Parse(rs.GetRange()); err != nil {
		name = ""
		return
	}
	lifetime = rs.GetLifetime().AsDuration()
	maxRetries = int(rs.GetMaxRetries())
	maxConcurrency = int(rs.GetMaxConcurrency())
	return
}

// FromID converts the given id to a *ID.
func FromID(id irange.ID) *ID {
	return &ID{Id: id.String()}
}

// ToID converts the given *ID to an irange.ID.
func ToID(id *ID) (irange.ID, error) {
	if id == nil {
		return irange.NilID, errors.New("nil *ID in ToID")
	}
	return idFromString(id.GetId())
}

// FromNames converts the given names into a *ListResponse.
func FromNames(names []string) *ListResponse {
	return &ListResponse{Names: names}
}

// ToNames converts the given *ListResponse into a slice of strings.
func ToNames(lr *ListResponse) ([]string, error) {
	if lr == nil {
		return nil, errors.New("nil *ListResponse in ToNames")
	}
	return lr.GetNames(), nil
}

// FromStatus converts the given *irange.Status into a *StatusResponse.
func FromStatus(s irange.Status) (*StatusResponse, error) {
	return &StatusResponse{
		Pending:   rangeToString(s.Pending()),
		Active:    rangeToString(s.Active()),
		Succeeded: rangeToString(s.Succeeded()),
		Failed:    rangeToString(s.Failed()),
	}, nil
}

// ToStatus converts the given *StatusResponse into an irange.Status.
func ToStatus(sr *StatusResponse) (irange.Status, error) {
	if sr == nil {
		return nil, errors.New("nil *StatusResponse in ToRanges")
	}
	pending, err := irange.Parse(sr.GetPending())
	if err != nil {
		return nil, err
	}
	active, err := irange.Parse(sr.GetActive())
	if err != nil {
		return nil, err
	}
	succeeded, err := irange.Parse(sr.GetSucceeded())
	if err != nil {
		return nil, err
	}
	failed, err := irange.Parse(sr.GetFailed())
	if err != nil {
		return nil, err
	}
	return &rStatus{
		pending:   pending,
		active:    active,
		succeeded: succeeded,
		failed:    failed,
	}, nil
}

// FromNameAndEntry converts the given name and entry to a *NameAndEntry
func FromNameAndEntry(name string, entry int64) *NameAndEntry {
	return &NameAndEntry{
		Name:  name,
		Entry: entry,
	}
}

// ToNameAndEntry converts the given *NameAndEntry to a string and an int64.
func ToNameAndEntry(ne *NameAndEntry) (name string, entry int64, err error) {
	if ne == nil {
		err = errors.New("nil *NameAndEntry in ToNameAndEntry")
		return
	}
	name = ne.GetName()
	entry = ne.GetEntry()
	return
}

// FromInfo converts the given irange.Info to a *InfoResponse.
func FromInfo(inf irange.Info) (*InfoResponse, error) {
	return &InfoResponse{
		Value:    inf.Value(),
		State:    uint32(inf.State()),
		Appname:  inf.AppName(),
		Hostname: inf.Hostname(),
		Start:    timestamppb.New(inf.Start()),
		Deadline: timestamppb.New(inf.Deadline()),
		Failures: int64(inf.Failures()),
	}, nil
}

// ToInfo converts the given *InfoResponse to an irange.Info.
func ToInfo(ir *InfoResponse) (irange.Info, error) {
	if ir == nil {
		return nil, errors.New("nil *InfoResponse in ToInfo")
	}
	// Assemble the result and return
	return &rInfo{
		value:    ir.GetValue(),
		state:    irange.State(ir.GetState()),
		appname:  ir.GetAppname(),
		hostname: ir.GetHostname(),
		start:    ir.GetStart().AsTime(),
		deadline: ir.GetDeadline().AsTime(),
		failures: int(ir.GetFailures()),
	}, nil
}
