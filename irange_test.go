// Tests for the irange package

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package irange

import (
	"bitbucket.org/pcas/irange/errors"
	"fmt"
	"github.com/stretchr/testify/require"
	"testing"
)

/////////////////////////////////////////////////////////////////////////
// Test functions
/////////////////////////////////////////////////////////////////////////

func requireEqualToString(r *require.Assertions, s string, R Range) {
	t := fmt.Sprintf("%s", R)
	r.Equal(s, t, "expected \"%s\" but got \"%s\"", s, t)
}

func requireEqualAsStrings(r *require.Assertions, R Range, T Range) {
	s := fmt.Sprintf("%s", R)
	t := fmt.Sprintf("%s", T)
	r.Equal(s, t, "expected \"%s\" but got \"%s\"", s, t)
}

// TestParseString tests the Parse and String methods.
func TestParseString(t *testing.T) {
	require := require.New(t)
	// Attempt to parse the test strings
	for _, S := range [][]string{
		{"[1..10]", "[1..10]"},
		{"  [ 1..10]  ", "[1..10]"},
		{"[-7,3..5]", "[-7,3..5]"},
		{"[-7,3,4,5]", "[-7,3..5]"},
		{"[-7,5..3]", "[-7]"},
		{"[]", "[]"},
		{"[-8..-12]", "[]"},
		{"", ""},
		{"[3,4,c,2]", ""},
		{"[3,4,5", ""},
		{"3,4,5]", ""},
		{"[-7, -5, -3, -4, -6]", "[-7..-3]"},
		{"[5..6,3..6..9]", ""},
		{"[4,x..12]", ""},
		{"[4,12..y]", ""},
		{"[3..4,6..7]", "[3..4,6..7]"},
		{"[3..4,5..7]", "[3..7]"},
		{"[3..6,4..7]", "[3..7]"},
		{"[4..6,4..7]", "[4..7]"},
		{"[3..8,4..7]", "[3..8]"},
		{"[4..8,3..7]", "[3..8]"},
		{"[4..8,2..3]", "[2..8]"},
		{"[5..8,2..3]", "[2..3,5..8]"},
	} {
		input, expected := S[0], S[1]
		R, err := Parse(input)
		if expected == "" {
			// We expect a parse error
			require.Error(err, "require error for %s but got nil", input)
			require.Equal(err, errors.ErrParse, "require ErrParse error for %s but got %v", err)
		} else {
			require.NoError(err, "unexpected parse error for %s", input)
			requireEqualToString(require, expected, R)
		}
	}
}

// sliceFromIterator returns a slice containing the contents of itr
func sliceFromIterator(itr Iterator) ([]int64, error) {
	var result []int64
	for itr.Next() {
		result = append(result, itr.Value())
	}
	return result, nil
}

// TestIterators tests the iterator methods
func TestIterators(t *testing.T) {
	require := require.New(t)
	// test the empty iterator
	for _, S := range []Strategy{First, Last, Compact, Uniform} {
		itr := S.Iterator(Empty)
		// taking the value without advancing should fail
		require.PanicsWithError(errors.ErrUninitialised.Error(), func() { itr.Value() })
		// advancing should fail
		require.False(itr.Next())
		require.NoError(itr.Err())
		// closing should succeed
		require.NoError(itr.Close())
		// taking the value of a closed iterator should fail
		require.PanicsWithError(errors.ErrClosed.Error(), func() { itr.Value() })
	}
	// test a non-empty iterator
	R, err := Parse("[3..5,-2,7,8]")
	require.NoError(err)
	var actual []int64
	// test the First strategy
	actual, err = sliceFromIterator(First.Iterator(R))
	require.NoError(err)
	require.Equal([]int64{-2, 3, 4, 5, 7, 8}, actual)
	// test the Last strategy
	actual, err = sliceFromIterator(Last.Iterator(R))
	require.NoError(err)
	require.Equal([]int64{8, 7, 5, 4, 3, -2}, actual)
	// test the Compact strategy
	actual, err = sliceFromIterator(Compact.Iterator(R))
	require.NoError(err)
	require.Equal([]int64{-2, 7, 8, 3, 4, 5}, actual)
	// test the Uniform strategy
	actual, err = sliceFromIterator(Uniform.Iterator(R))
	require.NoError(err)
	require.ElementsMatch([]int64{-2, 3, 4, 5, 7, 8}, actual)
	// reading from a closed iterator should fail
	itr := First.Iterator(R)
	require.NoError(itr.Close())
	require.False(itr.Next())
	err = itr.Err()
	require.Error(err)
	require.Equal(errors.ErrClosed, err)
}

// TestExclude tests exclude
func TestExclude(t *testing.T) {
	require := require.New(t)
	// prepare a Range
	R, err := Parse("[3..7,9]")
	require.NoError(err)
	// Excluding an element not in R should fail
	for _, n := range []int64{-1, 8, 12} {
		_, err := Exclude(R, n)
		require.Error(err)
		require.Equal(errors.ErrNotInRange, err)
	}
	_, err = Exclude(Empty, 0)
	require.Error(err)
	require.Equal(errors.ErrNotInRange, err)
	// Excluding values in R should succeed
	S, err := Exclude(R, 3)
	require.NoError(err)
	requireEqualToString(require, "[4..7,9]", S)
	S, err = Exclude(R, 4)
	require.NoError(err)
	requireEqualToString(require, "[3,5..7,9]", S)
	S, err = Exclude(R, 7)
	require.NoError(err)
	requireEqualToString(require, "[3..6,9]", S)
	S, err = Exclude(R, 9)
	require.NoError(err)
	requireEqualToString(require, "[3..7]", S)
}

// TestInclude tests include
func TestInclude(t *testing.T) {
	require := require.New(t)
	// prepare a Range
	R, err := Parse("[3..7,9]")
	require.NoError(err)
	// Including an element in R should give R
	for _, n := range []int64{3, 4, 7, 9} {
		S := Include(R, n)
		requireEqualAsStrings(require, R, S)
	}
	// Including values not in R should succeed
	S := Include(R, -1)
	requireEqualToString(require, "[-1,3..7,9]", S)
	S = Include(R, 2)
	requireEqualToString(require, "[2..7,9]", S)
	S = Include(R, 8)
	requireEqualAsStrings(require, ToInterval(3, 9), S)
	S = Include(R, 10)
	requireEqualToString(require, "[3..7,9..10]", S)
	S = Include(R, 100)
	requireEqualToString(require, "[3..7,9,100]", S)
}

// TestJoinMeetDiff tests Join, meet, and Diff
func TestJoinMeetDiff(t *testing.T) {
	require := require.New(t)
	// Prepare two ranges
	R, err := Parse("[1..3,8..12,15]")
	require.NoError(err)
	S, err := Parse("[2..6,12..13,17..20]")
	require.NoError(err)
	// Unions with Empty should work
	requireEqualAsStrings(require, Empty, Join(Empty, Empty))
	requireEqualAsStrings(require, R, Join(Empty, R))
	requireEqualAsStrings(require, R, Join(R, Empty))
	// Unions should work
	expected := "[1..6,8..13,15,17..20]"
	requireEqualToString(require, expected, Join(R, S))
	requireEqualToString(require, expected, Join(S, R))
	// Intersections with Empty should work
	requireEqualAsStrings(require, Empty, Meet(Empty, Empty))
	requireEqualAsStrings(require, Empty, Meet(Empty, R))
	requireEqualAsStrings(require, Empty, Meet(R, Empty))
	// Intersections should work
	expected = "[2..3,12]"
	requireEqualToString(require, expected, Meet(R, S))
	requireEqualToString(require, expected, Meet(S, R))
	// Diffs with Empty should work
	requireEqualAsStrings(require, Empty, Diff(Empty, Empty))
	requireEqualAsStrings(require, Empty, Diff(Empty, R))
	requireEqualAsStrings(require, R, Diff(R, Empty))
	// Diffs should work
	requireEqualToString(require, "[1,8..11,15]", Diff(R, S))
	requireEqualToString(require, "[4..6,13,17..20]", Diff(S, R))
}

// TestContents tests Contents.
func TestContents(t *testing.T) {
	require := require.New(t)
	// Prepare a range
	R, err := Parse("[-1..5,7..9,15]")
	require.NoError(err)
	require.ElementsMatch([]int64{-1, 0, 1, 2, 3, 4, 5, 7, 8, 9, 15}, Contents(R))
	// Test that empty ranges behave correctly
	for _, R := range []Range{Empty, ToInterval(9, 3), ToRange()} {
		require.Empty(Contents(R))
	}
}
