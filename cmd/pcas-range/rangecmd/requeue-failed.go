// Requeue-failed implements the "requeue-failed" command.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package rangecmd

import (
	"bitbucket.org/pcas/irange"
	"context"
	"errors"
	"flag"
	"fmt"
	"os"
	"text/tabwriter"
)

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// init registers the command.
func init() {
	register("requeue-failed", "", "Requeue failed entries in a range.", parseRequeueFailed)
}

// parseRequeueFailed parses the given arguments for the command, returning a run function on success.
func parseRequeueFailed(args []string) (RunFunc, error) {
	var name string
	// Create a new flag set
	f := flag.NewFlagSet("requeue-failed", flag.ExitOnError)
	f.Usage = usageRequeueFailed
	// Set the flags
	f.StringVar(&name, "name", name, "")
	// Parse the flags
	if err := f.Parse(args); err != nil {
		return nil, err
	}
	// Validate the options
	if name == "" {
		return nil, errors.New("a range name must be supplied")
	}
	// There should be no remaining arguments
	if f.NArg() != 0 {
		return nil, errors.New("too many arguments")
	}
	// Return the run function
	return createRequeueFailedFunc(name), nil
}

// usageRequeueFailed writes a usage message describing the arguments for the command.
func usageRequeueFailed() {
	fmt.Fprint(os.Stderr, `Command "requeue-failed" requeues all failed entries in a range.

Command usage: requeue-failed [flags]

Mandatory flags:
`)
	w := tabwriter.NewWriter(os.Stderr, 12, 1, 2, ' ', 0)
	fmt.Fprint(w, "  -name\tThe name of the range.\n")
	w.Flush()
}

// createRequeueFailedFunc returns a run function to requeue failed entries in a range.
func createRequeueFailedFunc(name string) RunFunc {
	return func(ctx context.Context, s irange.Storage) (err error) {
		return s.RequeueFailed(ctx, name)
	}
}
