// List implements the "list" command.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package rangecmd

import (
	"bitbucket.org/pcas/irange"
	"context"
	"errors"
	"flag"
	"fmt"
	"os"
)

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// init registers the command.
func init() {
	register("list", "", "List known ranges.", parseList)
}

// parseList parses the given arguments for the command, returning a run function on success.
func parseList(args []string) (RunFunc, error) {
	// Create a new flag set
	f := flag.NewFlagSet("list", flag.ExitOnError)
	f.Usage = usageList
	// There are no flags for the list command
	// Parse the flags
	if err := f.Parse(args); err != nil {
		return nil, err
	}
	// There should be no remaining arguments
	if f.NArg() != 0 {
		return nil, errors.New("too many arguments")
	}
	// Return the run function
	return createListFunc(), nil
}

// usageList writes a usage message describing the arguments for the command.
func usageList() {
	fmt.Fprint(os.Stderr, `Command "list" lists known ranges.

Command usage: list 

`)
}

// createListFunc returns a run function to list known ranges.
func createListFunc() RunFunc {
	return func(ctx context.Context, s irange.Storage) (err error) {
		var S []string
		S, err = s.List(ctx)
		if err != nil {
			return
		}
		for _, name := range S {
			fmt.Printf("%s\n", name)
		}
		return nil
	}
}
