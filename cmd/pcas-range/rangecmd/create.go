// Create implements the "create" command.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package rangecmd

import (
	"bitbucket.org/pcas/irange"
	"context"
	"errors"
	"flag"
	"fmt"
	"math"
	"os"
	"text/tabwriter"
	"time"
)

// Default values for options
const (
	DefaultMaxRetries     = 7
	DefaultMaxConcurrency = math.MaxInt16
)

// createOptions describe the options for "create".
type createOptions struct {
	Name           string        // The range name
	RangeSpec      string        // The range specification
	Lifetime       time.Duration // The lifetime of entries
	MaxRetries     int           // The maximum number of times that an entry may be retried
	MaxConcurrency int           // The maximum number of simultaneously active entries
}

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// init registers the command.
func init() {
	register("create", "", "Create a range.", parseCreate)
}

// defaultOptions returns default options for the create command
func defaultOptions() *createOptions {
	return &createOptions{
		MaxRetries:     DefaultMaxRetries,
		MaxConcurrency: DefaultMaxConcurrency,
	}
}

// validate validates the options.
func validate(opts *createOptions) error {
	if opts.Name == "" {
		return errors.New("a range name must be supplied")
	} else if opts.RangeSpec == "" {
		return errors.New("a range must be supplied")
	} else if opts.Lifetime <= 0 {
		return errors.New("the lifetime must be positive")
	}
	return nil
}

// parseCreate parses the given arguments for the command, returning a run function on success.
func parseCreate(args []string) (RunFunc, error) {
	// Create a new flag set
	f := flag.NewFlagSet("create", flag.ExitOnError)
	f.Usage = usageCreate
	// Set the flags
	opts := defaultOptions()
	f.StringVar(&opts.Name, "name", opts.Name, "")
	f.StringVar(&opts.RangeSpec, "range", opts.RangeSpec, "")
	f.DurationVar(&opts.Lifetime, "lifetime", opts.Lifetime, "")
	f.IntVar(&opts.MaxRetries, "maxRetries", opts.MaxRetries, "")
	f.IntVar(&opts.MaxConcurrency, "maxConcurrency", opts.MaxConcurrency, "")
	// Parse the flags
	if err := f.Parse(args); err != nil {
		return nil, err
	}
	// Validate the options
	if err := validate(opts); err != nil {
		return nil, err
	}
	// There should be no remaining arguments
	if f.NArg() != 0 {
		return nil, errors.New("too many arguments")
	}
	// Return the run function
	return createCreateFunc(opts), nil
}

// usageCreate writes a usage message describing the arguments for the command.
func usageCreate() {
	fmt.Fprint(os.Stderr, `Command "create" creates a new range.

Command usage: create [flags] 

Mandatory flags:
`)
	w := tabwriter.NewWriter(os.Stderr, 12, 1, 2, ' ', 0)
	fmt.Fprint(w, "  -lifetime\tThe lifetime of entries.\n")
	fmt.Fprint(w, "  -name\tThe name of the range.\n")
	fmt.Fprint(w, "  -range\tThe range, e.g. \"[-5..-3,2,12..13]\".\n")
	w.Flush()
	fmt.Fprint(os.Stderr, `
Optional flags:
`)
	fmt.Fprintf(w, "  -maxConcurrency\tThe maximum number of simultaneously active entries (default: %d).\n", DefaultMaxConcurrency)
	fmt.Fprintf(w, "  -maxRetries\tThe maximum number of times that an entry may be retried (default: %d).\n", DefaultMaxRetries)
	w.Flush()
}

// createCreateFunc returns a run function to create a range.
func createCreateFunc(opts *createOptions) RunFunc {
	return func(ctx context.Context, s irange.Storage) (err error) {
		R, err := irange.Parse(opts.RangeSpec)
		if err != nil {
			return err
		}
		return s.Create(ctx, opts.Name, R, opts.Lifetime, opts.MaxRetries, opts.MaxConcurrency)
	}
}
