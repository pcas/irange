// Delete implements the "delete" command.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package rangecmd

import (
	"bitbucket.org/pcas/irange"
	"context"
	"errors"
	"flag"
	"fmt"
	"os"
)

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// init registers the command.
func init() {
	register("delete", "", "Delete a range.", parseDelete)
}

// parseDelete parses the given arguments for the command, returning a run function on success.
func parseDelete(args []string) (RunFunc, error) {
	var name string
	// Create a new flag set
	f := flag.NewFlagSet("delete", flag.ExitOnError)
	f.Usage = usageDelete
	// Set the flags
	f.StringVar(&name, "name", name, "")
	// Parse the flags
	if err := f.Parse(args); err != nil {
		return nil, err
	}
	// Validate the options
	if name == "" {
		return nil, errors.New("a range name must be supplied")
	}
	// There should be no remaining arguments
	if f.NArg() != 0 {
		return nil, errors.New("too many arguments")
	}
	// Return the run function
	return createDeleteFunc(name), nil
}

// usageDelete writes a usage message describing the arguments for the command.
func usageDelete() {
	fmt.Fprint(os.Stderr, `Command "delete" deletes a range.

Command usage: delete -name=rangeName

`)
}

// createDeleteFunc returns a run function to delete a range.
func createDeleteFunc(name string) RunFunc {
	return func(ctx context.Context, s irange.Storage) (err error) {
		return s.Delete(ctx, name)
	}
}
