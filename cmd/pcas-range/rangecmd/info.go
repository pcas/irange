// Info implements the "info" command.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package rangecmd

import (
	"context"
	"errors"
	"flag"
	"fmt"
	"os"
	"strconv"

	"bitbucket.org/pcas/irange"
)

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// init registers the command.
func init() {
	register("info", "", "Get status information for a value in a range.", parseInfo)
}

// parseInfo parses the given arguments for the command, returning a run function on success.
func parseInfo(args []string) (RunFunc, error) {
	var name string
	var valueSpec string
	var n int64
	// Create a new flag set
	f := flag.NewFlagSet("info", flag.ExitOnError)
	f.Usage = usageInfo
	// Set the flags
	f.StringVar(&name, "name", name, "")
	f.StringVar(&valueSpec, "value", valueSpec, "")
	// Parse the flags
	if err := f.Parse(args); err != nil {
		return nil, err
	}
	// Validate the options
	var err error
	if name == "" {
		return nil, errors.New("a range name must be supplied")
	} else if valueSpec == "" {
		return nil, errors.New("a value must be supplied")
	} else if n, err = strconv.ParseInt(valueSpec, 10, 64); err != nil {
		return nil, fmt.Errorf("errors parsing value: %w", err)
	}
	// There should be no remaining arguments
	if f.NArg() != 0 {
		return nil, errors.New("too many arguments")
	}
	// Return the run function
	return createInfoFunc(name, n), nil
}

// usageInfo writes a usage message describing the arguments for the command.
func usageInfo() {
	fmt.Fprint(os.Stderr, `Command "info" gets status information for a value in a range.

Command usage: info -name=rangeName -value=n

`)
}

// createInfoFunc returns a run function to get status information for a value in a range.
func createInfoFunc(name string, n int64) RunFunc {
	return func(ctx context.Context, s irange.Storage) (err error) {
		x, err := s.Info(ctx, name, n)
		if err != nil {
			return err
		}
		fmt.Printf("State: %s\n", x.State())
		if x.State() == irange.Active {
			fmt.Printf("AppName: %s\n", x.AppName())
			fmt.Printf("Hostname: %s\n", x.Hostname())
			fmt.Printf("Start time: %s\n", x.Start())
			fmt.Printf("Deadline: %v\n", x.Deadline())
		}
		return nil
	}
}
