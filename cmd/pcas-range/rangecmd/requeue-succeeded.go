// Requeue-succeeded implements the "requeue-succeeded" command.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package rangecmd

import (
	"bitbucket.org/pcas/irange"
	"context"
	"errors"
	"flag"
	"fmt"
	"os"
	"text/tabwriter"
)

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// init registers the command.
func init() {
	register("requeue-succeeded", "", "Requeue succeeded entries in a range.", parseRequeueSucceeded)
}

// parseRequeueSucceeded parses the given arguments for the command, returning a run function on success.
func parseRequeueSucceeded(args []string) (RunFunc, error) {
	var name string
	// Create a new flag set
	f := flag.NewFlagSet("requeue-succeeded", flag.ExitOnError)
	f.Usage = usageRequeueSucceeded
	// Set the flags
	f.StringVar(&name, "name", name, "")
	// Parse the flags
	if err := f.Parse(args); err != nil {
		return nil, err
	}
	// Validate the options
	if name == "" {
		return nil, errors.New("a range name must be supplied")
	}
	// There should be no remaining arguments
	if f.NArg() != 0 {
		return nil, errors.New("too many arguments")
	}
	// Return the run function
	return createRequeueSucceededFunc(name), nil
}

// usageRequeueSucceeded writes a usage message describing the arguments for the command.
func usageRequeueSucceeded() {
	fmt.Fprint(os.Stderr, `Command "requeue-succeeded" requeues all succeeded entries in a range.

Command usage: requeue-succeeded [flags]

Mandatory flags:
`)
	w := tabwriter.NewWriter(os.Stderr, 12, 1, 2, ' ', 0)
	fmt.Fprint(w, "  -name\tThe name of the range.\n")
	w.Flush()
}

// createRequeueSucceededFunc returns a run function to requeue succeeded entries in a range.
func createRequeueSucceededFunc(name string) RunFunc {
	return func(ctx context.Context, s irange.Storage) (err error) {
		return s.RequeueSucceeded(ctx, name)
	}
}
