// Rangecmd implements commands for pcas-range.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package rangecmd

import (
	"context"
	"sort"

	"bitbucket.org/pcas/irange"
)

// Cmd represents a command.
type Cmd struct {
	name     string
	longName string
	desc     string
	parse    parseFunc
}

// RunFunc is a function that can be run on the given storage to execute a command.
type RunFunc func(context.Context, irange.Storage) error

// parseFunc is a parse function for a command. The command-line arguments will be passed in 'args'. The parse function should return a run function.
type parseFunc func(args []string) (RunFunc, error)

// registry contains the registered commands.
var registry map[string]*Cmd

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// register registers the a new command with given name, description, and parse function. This will panic if the name is already in use.
func register(name string, longName string, desc string, parse parseFunc) {
	if registry == nil {
		registry = make(map[string]*Cmd)
	}
	if _, ok := registry[name]; ok {
		panic("Command already registered with name " + name)
	}
	registry[name] = &Cmd{
		name:     name,
		longName: longName,
		desc:     desc,
		parse:    parse,
	}
}

/////////////////////////////////////////////////////////////////////////
// Cmd functions
/////////////////////////////////////////////////////////////////////////

// String returns "[Long name, ]Name\tDescription".
func (c *Cmd) String() string {
	name, longName := c.Name(), c.LongName()
	if name == longName {
		return name + "\t" + c.Description()
	}
	return longName + ", " + name + "\t" + c.Description()
}

// Name returns the short form of the command name.
func (c *Cmd) Name() string {
	if c == nil {
		return ""
	}
	return c.name
}

// LongName returns the long form of the command name. If no long form is set, this will be the same as the short form returned by Name.
func (c *Cmd) LongName() string {
	if c == nil {
		return ""
	} else if len(c.longName) == 0 {
		return c.name
	}
	return c.longName
}

// Description returns a brief one-line description of the command.
func (c *Cmd) Description() string {
	if c == nil {
		return ""
	}
	return c.desc
}

// Parse attempts to generate a run function from the given command-line arguments.
func (c *Cmd) Parse(args []string) (RunFunc, error) {
	// Sanity check
	if c == nil || c.parse == nil {
		return nil, nil
	}
	// Create the run function and return
	f, err := c.parse(args)
	if err != nil {
		return nil, err
	}
	return f, nil
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// Get returns the command with the given name, if any.
func Get(name string) (*Cmd, bool) {
	// Is a command registered with this name?
	c, ok := registry[name]
	if ok {
		return c, true
	}
	// It's possible this matches the long name of a command
	for _, c := range registry {
		if c.LongName() == name {
			return c, true
		}
	}
	// No luck
	return nil, false
}

// Cmds returns a slice of all available commands, sorted in increasing order by long name.
func Cmds() []*Cmd {
	// Create the slice of available commands
	cmds := make([]*Cmd, 0, len(registry))
	for _, c := range registry {
		cmds = append(cmds, c)
	}
	// Sort the slice and return
	sort.Slice(cmds, func(i int, j int) bool {
		return cmds[i].LongName() < cmds[j].LongName()
	})
	return cmds
}
