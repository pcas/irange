// Status implements the "status" command.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package rangecmd

import (
	"bitbucket.org/pcas/irange"
	"context"
	"errors"
	"flag"
	"fmt"
	"os"
	"text/tabwriter"
)

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// init registers the command.
func init() {
	register("status", "", "Get the status of a range.", parseStatus)
}

// parseStatus parses the given arguments for the command, returning a run function on success.
func parseStatus(args []string) (RunFunc, error) {
	var name string
	var summary bool
	// Create a new flag set
	f := flag.NewFlagSet("status", flag.ExitOnError)
	f.Usage = usageStatus
	// Set the flags
	f.StringVar(&name, "name", name, "")
	f.BoolVar(&summary, "summary", summary, "")
	// Parse the flags
	if err := f.Parse(args); err != nil {
		return nil, err
	}
	// Validate the options
	if name == "" {
		return nil, errors.New("a range name must be supplied")
	}
	// There should be no remaining arguments
	if f.NArg() != 0 {
		return nil, errors.New("too many arguments")
	}
	// Return the run function
	return createStatusFunc(name, summary), nil
}

// usageStatus writes a usage message describing the arguments for the command.
func usageStatus() {
	fmt.Fprint(os.Stderr, `Command "status" gets the status of a range.

Command usage: status [flags]

Mandatory flags:
`)
	w := tabwriter.NewWriter(os.Stderr, 12, 1, 2, ' ', 0)
	fmt.Fprint(w, "  -name\tThe name of the range.\n")
	w.Flush()
	fmt.Fprint(os.Stderr, `
Optional flags:
`)
	fmt.Fprint(w, "  -summary\tSuppress detailed description of ranges.\n")
	w.Flush()
}

// createStatusFunc returns a run function to get the status of a range.
func createStatusFunc(name string, summary bool) RunFunc {
	return func(ctx context.Context, s irange.Storage) (err error) {
		st, err := s.Status(ctx, name)
		if err != nil {
			return err
		}
		if summary {
			fmt.Printf("Pending: %d\n", irange.Len(st.Pending()))
			fmt.Printf("Active: %d\n", irange.Len(st.Active()))
			fmt.Printf("Succeeded: %d\n", irange.Len(st.Succeeded()))
			fmt.Printf("Failed: %d\n", irange.Len(st.Failed()))
			return nil
		}
		fmt.Printf("Pending: %s\n", st.Pending())
		fmt.Printf("Active: %s\n", st.Active())
		fmt.Printf("Succeeded: %s\n", st.Succeeded())
		fmt.Printf("Failed: %s\n", st.Failed())
		return nil
	}
}
