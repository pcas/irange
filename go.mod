module bitbucket.org/pcas/irange

go 1.14

require (
	bitbucket.org/pcas/logger v0.1.43
	bitbucket.org/pcas/metrics v0.1.35
	bitbucket.org/pcas/sslflag v0.0.16
	bitbucket.org/pcastools/address v0.1.4
	bitbucket.org/pcastools/bytesbuffer v1.0.3
	bitbucket.org/pcastools/cleanup v1.0.4
	bitbucket.org/pcastools/flag v0.0.19
	bitbucket.org/pcastools/grpcutil v1.0.14
	bitbucket.org/pcastools/hash v1.0.5
	bitbucket.org/pcastools/listenutil v0.0.10
	bitbucket.org/pcastools/log v1.0.4
	bitbucket.org/pcastools/stringsbuilder v1.0.3
	bitbucket.org/pcastools/timeutil v0.1.3
	bitbucket.org/pcastools/ulid v0.1.6
	bitbucket.org/pcastools/version v0.0.5
	github.com/grpc-ecosystem/go-grpc-middleware v1.3.0
	github.com/petar/GoLLRB v0.0.0-20210522233825-ae3b015fd3e9 // indirect
	github.com/stretchr/testify v1.8.1
	google.golang.org/grpc v1.54.0
	google.golang.org/protobuf v1.30.0
)
