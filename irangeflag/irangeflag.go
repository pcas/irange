// Irangeflag defines a flag type for parsing ranges as command-line arguments

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package irangeflag

import (
	"bitbucket.org/pcas/irange"
	"bitbucket.org/pcastools/flag"
	"fmt"
)

/////////////////////////////////////////////////////////////////////////
// rangeFlag functions
/////////////////////////////////////////////////////////////////////////

// rangeFlag implements pcastools/flag.Flag and defines a Range-valued flag.
type rangeFlag struct {
	name        string        // The name of the flag.
	description string        // The short-form help text for this flag.
	usage       string        // The long-form help text for this flag.
	r           *irange.Range // The variable associated to this flag.
}

// Name returns the flag name (without leading hyphen).
func (f *rangeFlag) Name() string {
	return f.name
}

// Description returns a one-line description of this variable.
func (f *rangeFlag) Description() string {
	return fmt.Sprintf("%s (default: %s)", f.description, *f.r)
}

// Usage returns long-form usage text (or the empty string if not set).
func (f *rangeFlag) Usage() string {
	return f.usage
}

// Parse parses the string.
func (f *rangeFlag) Parse(in string) error {
	result, err := irange.Parse(in)
	if err != nil {
		return err
	}
	*(f.r) = result
	return nil
}

// New returns a pcastools/flag.Flag that represents a Range-valued flag with the given name, description, and usage string. It has backing variable R, and default value def.
func New(name string, R *irange.Range, def irange.Range, description string, usage string) flag.Flag {
	result := &rangeFlag{
		name:        name,
		description: description,
		usage:       usage,
		r:           R,
	}
	*(result.r) = def
	return result
}
